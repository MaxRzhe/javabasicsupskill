package classes.composition.textapp;

import java.util.ArrayList;
import java.util.Arrays;

//Создать объект класса Текст, используя классы Предложение, Слово.
//Методы: дополнить текст, вывести на консоль текст, заголовок текста.
public class TextApp {

    public static void main(String[] args) {
        Text text = TextData.load();

        Sentence sentence = new Sentence();
        sentence.setParts(new ArrayList<>(Arrays.asList(
                Gap.SPACE,
                new Word("Java"),
                Gap.SPACE,
                new Word("is"),
                Gap.SPACE,
                new Word("cool"),
                Punctuation.EXCLAMATION,
                Gap.LINE_BREAK
        )));

        text.addSentence(sentence);

        System.out.println(text);

        System.out.println(text.getTitle());


    }
}
