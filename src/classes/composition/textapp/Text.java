package classes.composition.textapp;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Text {
    private LinkedList<Sentence> sentences;

    public LinkedList<Sentence> getSentences() {
        return sentences == null ? new LinkedList<>() : sentences;
    }

    public void setSentences(List<Sentence> sentences) {
        this.sentences = new LinkedList<>(sentences);
    }

    public void setTitle(Sentence sentence) {
        if (sentences == null) {
            sentences = new LinkedList<>();
        }
        sentences.addFirst(sentence);
    }

    public Sentence getTitle() {
        return sentences.getFirst();
    }

    public void addSentence(Sentence sentence) {
        if (sentences == null) {
            sentences = new LinkedList<>();
        }
        sentences.addLast(sentence);
    }

    @Override
    public String toString() {
        return sentences.stream()
                .map(Sentence::toString)
                .collect(Collectors.joining());
    }
}
