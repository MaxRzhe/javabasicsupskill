package classes.composition.textapp;

public enum Punctuation implements Textable {
    DOT("."),
    COMMA(","),
    DASH("-"),
    EXCLAMATION("!"),
    QUESTION("?"),
    QUOTATION("\""),
    OPEN_PARENTHESIS("("),
    CLOSING_PARENTHESIS(")");

    private final String value;

    Punctuation(String value) {
        this.value = value;
    }

    @Override
    public String value() {
        return value;
    }
}
