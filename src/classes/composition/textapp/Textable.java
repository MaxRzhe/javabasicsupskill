package classes.composition.textapp;

public interface Textable {
    String value();
}
