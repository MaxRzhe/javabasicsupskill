package classes.composition.textapp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class TextData {

    public static Text load() {
        Gap tabulation = Gap.TABULATION;
        Punctuation dash = Punctuation.DASH;
        Punctuation dot = Punctuation.DOT;
        Punctuation comma = Punctuation.COMMA;
        Gap space = Gap.SPACE;
        Gap lineBreak = Gap.LINE_BREAK;

        Word word1 = new Word("java");
        word1.capitalizeFirstLetter();
        Word word2 = new Word("is");
        Word word3 = new Word("a");
        Word word4 = new Word("class");
        Word word5 = new Word("based");
        Word word6 = new Word("object");
        Word word7 = new Word("oriented");
        Word word8 = new Word("language");

        List<Textable> parts = new ArrayList<>(Arrays.asList(word1, space, word2, space, word3, space, word4,
                dash, word5, space, word6, dash, word7, space, word8, dot, lineBreak));
        Sentence sentence1 = new Sentence();
        sentence1.setParts(parts);

        Word word9 = new Word("the");
        word9.capitalizeFirstLetter();
        Word word10 = new Word("latest");
        Word word11 = new Word("version");
        Word word12 = new Word("is");
        Word word13 = new Word("java");
        word13.capitalizeFirstLetter();
        Word word14 = new Word("15");
        Word word15 = new Word("released");
        Word word16 = new Word("in");
        Word word17 = new Word("september");
        word17.capitalizeFirstLetter();
        Word word18 = new Word("2020");

        List<Textable> parts2 = new ArrayList<>(Arrays.asList(
                word9, space, word10, space, word11, space, word12, space, word13, space, word14, comma, space, word15, space,
                word16, space, word17, space, word18, dot));
        Sentence sentence2 = new Sentence();
        sentence2.setParts(parts2);

        Word word19 = new Word("about");
        word19.toUpperCase();
        Word word20 = new Word("java");
        word20.toUpperCase();

        List<Textable> parts3 = new ArrayList<>(Arrays.asList(tabulation, word19, space, word20, dot, lineBreak));
        Sentence title = new Sentence();
        title.setParts(parts3);

        Text text = new Text();
        text.setSentences(new ArrayList<>(Arrays.asList(sentence1, sentence2)));
        text.setTitle(title);

        return text;
    }
}
