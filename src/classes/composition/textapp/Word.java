package classes.composition.textapp;

public class Word implements Textable{
    private String value;

    public Word(String value) {
        this.value = value;

    }

    @Override
    public String value() {
        return value;
    }

    public void capitalizeFirstLetter() {
        value = Character.toUpperCase(value.charAt(0)) + value.substring(1);
    }

    public void toUpperCase() {
        value = value.toUpperCase();
    }

    public void toLowerCase() {
        value = value.toLowerCase();
    }
}
