package classes.composition.textapp;

public enum Gap implements Textable {
    SPACE("\u0020"),
    TABULATION("\t"),
    LINE_BREAK("\n");

    private final String value;

    Gap(String value) {
        this.value = value;
    }

    @Override
    public String value() {
        return value;
    }
}
