package classes.composition.textapp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Sentence {
    private List<Textable> parts;

    public Sentence() {
        parts = new ArrayList<>();
    }

    public List<Textable> getParts() {
        return parts;
    }

    public void setParts(List<Textable> parts) {
        this.parts = parts;
    }

    public boolean addPart(Textable textable) {
        return parts.add(textable);
    }

    public boolean removeFirstFound(Textable textable) {
        return parts.remove(textable);
    }

    @Override
    public String toString() {
        return parts.stream()
                .map(Textable::value)
                .collect(Collectors.joining());
    }

}
