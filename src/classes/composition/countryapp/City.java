package classes.composition.countryapp;

import java.util.Objects;

public class City {
    private String name;
    private boolean isCapital;
    private boolean isRegionalCenter;
    private boolean isDistrictCenter;

    public City(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCapital() {
        return isCapital;
    }

    public void setCapital(boolean capital) {
        isCapital = capital;
    }

    public boolean isRegionalCenter() {
        return isRegionalCenter;
    }

    public void setRegionalCenter(boolean regionalCenter) {
        isRegionalCenter = regionalCenter;
    }

    public boolean isDistrictCenter() {
        return isDistrictCenter;
    }

    public void setDistrictCenter(boolean districtCenter) {
        isDistrictCenter = districtCenter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return isCapital() == city.isCapital() &&
                isRegionalCenter() == city.isRegionalCenter() &&
                isDistrictCenter() == city.isDistrictCenter() &&
                getName().equals(city.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), isCapital(), isRegionalCenter(), isDistrictCenter());
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", isCapital=" + isCapital +
                ", isRegionalCenter=" + isRegionalCenter +
                ", isDistrictCenter=" + isDistrictCenter +
                '}';
    }
}
