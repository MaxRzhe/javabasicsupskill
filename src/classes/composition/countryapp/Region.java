package classes.composition.countryapp;

import java.util.ArrayList;
import java.util.List;

public class Region {
    private String name;
    private List<District> districts;

    public Region(String name) {
        this.name = name;
        this.districts = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    public boolean addDistrict(District district) {
        if (districts.contains(district)) {
            return false;
        } else {
            return districts.add(district);
        }
    }

    public boolean removeDistrict(District district) {
        return districts.remove(district);
    }

    public double getArea() {
        return districts.size() == 0 ? 0.0 : districts.stream().mapToDouble(District::getArea).sum();
    }
}
