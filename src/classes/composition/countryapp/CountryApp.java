package classes.composition.countryapp;


//Создать объект класса Государство, используя классы Область, Район, Город.
//Методы: вывести на консоль столицу, количество областей, площадь, областные центры.
public class CountryApp {

    public static void main(String[] args) {
        Country country = CountryData.loadData();

        System.out.printf("Country name: %s\n", country.getName());
        System.out.printf("Capital: %s\n", country.getCapital());
        System.out.printf("Area: %.2f km2\n", country.getArea());
        System.out.printf("Regions number: %d\n", country.regionsCount());
        System.out.printf("Region centers: %s\n", country.findRegionalCenters());
        System.out.printf("Regions info: %s\n", country.getRegions());
    }
}
