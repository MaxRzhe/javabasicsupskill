package classes.composition.countryapp;

import java.util.ArrayList;
import java.util.List;

public class District {
    private String name;
    private double area;
    private List<City> cities;

    public District(String name, double area) {
        this.name = name;
        this.area = area;
        this.cities = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getArea() {
        return area;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public boolean addCity(City city) {
        if (cities.contains(city)) {
            return false;
        } else {
            return cities.add(city);
        }
    }

    public boolean removeCity(City city) {
        return cities.remove(city);
    }

    @Override
    public String toString() {
        return "District{" +
                "name='" + name + '\'' +
                ", area=" + area +
                ", cities=" + cities +
                '}';
    }
}
