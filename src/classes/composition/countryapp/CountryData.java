package classes.composition.countryapp;

import java.util.ArrayList;
import java.util.Arrays;

public class CountryData {

    public static Country loadData() {
        City city1 = new City("Rome");
        city1.setCapital(true);
        city1.setRegionalCenter(true);
        city1.setDistrictCenter(true);
        City city2 = new City("Fiumicino");
        City city3 = new City("Aprilia");

        City city4 = new City("Frosinone");
        city4.setDistrictCenter(true);
        City city5 = new City("Viterbo");
        City city6 = new City("Pomezia");

        City city7 = new City("Latina");
        city7.setDistrictCenter(true);
        City city8 = new City("Anzio");
        City city9 = new City("Tivoli");

        City city10 = new City("Milan");
        city10.setDistrictCenter(true);
        city10.setRegionalCenter(true);
        City city11 = new City("Bergamo");
        City city12 = new City("Como");

        City city13 = new City("Brescia");
        city13.setDistrictCenter(true);
        City city14 = new City("Monza");
        City city15 = new City("Busto Arsizio");

        City city16 = new City("Cremona");
        city16.setDistrictCenter(true);
        City city17 = new City("Pavia");
        City city18 = new City("Vigevano");

        City city19 = new City("Naples");
        city19.setDistrictCenter(true);
        city19.setRegionalCenter(true);
        City city20 = new City("Pozzuoli");
        City city21 = new City("Giugliano in Campania");

        City city22 = new City("Salerno");
        city22.setDistrictCenter(true);
        City city23 = new City("Torre del Greco");
        City city24 = new City("Afragola");

        City city25 = new City("Casoria");
        city25.setDistrictCenter(true);
        City city26 = new City("Castellammare di Stabia");
        City city27 = new City("Acerra");


        District district1 = new District("Rome Capital", 5352);
        district1.addCity(city1);
        district1.addCity(city2);
        district1.addCity(city3);
        District district2 = new District("Frosinone", 3244);
        district2.addCity(city4);
        district2.addCity(city5);
        district2.addCity(city6);

        District district3 = new District("Latina", 2251);
        district3.addCity(city7);
        district3.addCity(city8);
        district3.addCity(city9);

        District district4 = new District("Bergamo", 2723);
        district4.addCity(city10);
        district4.addCity(city11);
        district4.addCity(city12);

        District district5 = new District("Brescia", 4784);
        district5.addCity(city13);
        district5.addCity(city14);
        district5.addCity(city15);

        District district6 = new District("Cremona", 1772);
        district6.addCity(city16);
        district6.addCity(city17);
        district6.addCity(city18);

        District district7 = new District("Avellino", 2792);
        district7.addCity(city19);
        district7.addCity(city20);
        district7.addCity(city21);

        District district8 = new District("Salerno", 4923);
        district8.addCity(city22);
        district8.addCity(city23);
        district8.addCity(city24);

        District district9 = new District("Caserta", 2639);
        district9.addCity(city25);
        district9.addCity(city26);
        district9.addCity(city27);

        Region region1 = new Region("Lazio");
        region1.setDistricts(new ArrayList<>(Arrays.asList(district1, district2, district3)));
        Region region2 = new Region("Lombardy");
        region2.setDistricts(new ArrayList<>(Arrays.asList(district4, district5, district6)));
        Region region3 = new Region("Campania");
        region3.setDistricts(new ArrayList<>(Arrays.asList(district7, district8, district9)));

        Country country = new Country("Italia");
        country.setRegions(new ArrayList<>(Arrays.asList(region1, region2, region3)));

        return country;
    }

}
