package classes.composition.countryapp;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Country {
    private String name;
    private City capital;
    private List<Region> regions;

    public Country(String name) {
        this.name = name;
        this.regions = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCapital() {
        return this.capital == null ? findCapital() : this.capital;
    }

    public void setCapital(City capital) {
        this.capital = capital;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    public int regionsCount() {
       return regions.size();
    }

    public double getArea() {
        return regions.size() != 0 ? regions.stream().mapToDouble(Region::getArea).sum() : 0;
    }

    public List<City> findRegionalCenters() {
        List<City> centers = new ArrayList<>();
        if (regions.size() != 0) {
            for (Region region : regions) {
                List<District> districts = region.getDistricts();
                if (districts.size() != 0) {
                    for (District district : districts) {
                        List<City> cities = district.getCities();
                        if (cities.size() != 0) {
                            centers.addAll(cities.stream().filter(City::isRegionalCenter).collect(Collectors.toList()));
                        }
                    }
                }
            }
        }
        return centers;
    }

    private City findCapital() {
        List<City> citiesList = new ArrayList<>();
        if (regions.size() != 0) {
            for (Region region : regions) {
                List<District> districts = region.getDistricts();
                if (districts.size() != 0) {
                    for (District district : districts) {
                        List<City> cities = district.getCities();
                        if (cities.size() != 0) {
                            citiesList.add(cities.stream().filter(City::isCapital).findFirst().orElse(null));
                        }
                    }
                }
            }
        }
        return citiesList.get(0);
    }
}
