package classes.composition.carapp;

public class Wheel {
    private boolean isPunctured;

    public Wheel() {
        this.isPunctured = false;
    }

    public boolean isPunctured() {
        return isPunctured;
    }

    public void setPunctured(boolean punctured) {
        isPunctured = punctured;
        if (isPunctured) {
            System.out.println("Some wheel is punctured");
        }
    }
}
