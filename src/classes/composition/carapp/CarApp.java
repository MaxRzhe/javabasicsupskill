package classes.composition.carapp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


//Создать объект класса Автомобиль, используя классы Колесо, Двигатель.
//Методы: ехать, заправляться, менять колесо, вывести на консоль марку автомобиля.
public class CarApp {

    public static void main(String[] args) {
        List<Wheel> wheels = new ArrayList<>(Arrays.asList(new Wheel(), new Wheel(), new Wheel(), new Wheel()));
        Engine engine = new Engine();

        Car car = new Car("Ford", engine, wheels);
        System.out.println("Car model: " + car.getModel());

        car.fillTank(30.0);

        car.startEngine();
        car.move();

        car.getWheels().get(2).setPunctured(true);
        
        car.stop();
        car.stopEngine();

        car.searchPuncturedWheel();
        car.changeWheel();

        car.startEngine();
        car.move();
    }
}
