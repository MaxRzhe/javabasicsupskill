package classes.composition.carapp;

public class Engine {
    private boolean isStarted;

    public Engine() {
        this.isStarted = false;
    }

    public boolean isStarted() {
        return isStarted;
    }

    public void start() {
        isStarted = true;
        System.out.println("Engine started");
    }

    public void stop() {
        isStarted = false;
        System.out.println("Engine stopped");
    }
}
