package classes.composition.carapp;

import java.util.List;

public class Car {
    private final String model;
    private final Engine engine;
    private final List<Wheel> wheels;
    private double petrol;
    private final double tankVolume;

    public Car(String model, Engine engine, List<Wheel> wheels) {
        this.model = model;
        this.engine = engine;
        this.wheels = wheels;
        this.petrol = 0.0;
        this.tankVolume = 50.0;
    }

    public String getModel() {
        return model;
    }

    public List<Wheel> getWheels() {
        return wheels;
    }

    public double getPetrol() {
        return petrol;
    }

    public double getTankVolume() {
        return tankVolume;
    }

    public void startEngine() {
        if (petrol < 5.0) {
            System.out.println("Fill the tank. Low petrol level");
        } else if (!this.engine.isStarted()) {
            this.engine.start();
        }
    }

    public void stopEngine() {
        if (this.engine.isStarted()) {
            this.engine.stop();
        }
    }

    public void move() {
        if (petrol < 5) {
            System.out.println("Fill the tank. Low petrol level");
        } else {
            System.out.println("Car started moving");
        }
    }

    public void stop() {
        if (petrol < 5) {
            System.out.println("Fill the tank. Low petrol level");
        }
        System.out.println("Car stopped");
    }

    public double fillTank(double petrol) {
        this.petrol += petrol;
        if (this.petrol > this.tankVolume) {
            double over = this.petrol % this.tankVolume;
            this.petrol = this.tankVolume;
            System.out.println("Full tank");
            return over;
        } else {
            return 0.0;
        }

    }

    public void searchPuncturedWheel() {
        for (Wheel wheel : getWheels()) {
            if (wheel.isPunctured()) {
                System.out.printf("The wheel #%d is punctured!\n", getWheels().indexOf(wheel) + 1);
                break;
            }
        }
    }

    public void changeWheel() {
        for (Wheel wheel : getWheels()) {
            if (wheel.isPunctured()) {
                getWheels().set(getWheels().indexOf(wheel), new Wheel());
                System.out.println("The wheel was replaced!");
            }
        }
    }


}
