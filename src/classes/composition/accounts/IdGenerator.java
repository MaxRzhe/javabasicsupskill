package classes.composition.accounts;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class IdGenerator {
    private static final List<Long> cache = new ArrayList<>();

    public static long generate() {
        long id;
        while (true) {
            Random random = new Random();
            id = random.nextLong();
            if (id > 0 && !cache.contains(id)) {
               cache.add(id);
               return id;
            }
        }
    }
}
