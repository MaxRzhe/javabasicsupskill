package classes.composition.accounts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Bank {
    private String name;
    private final List<Client> clients;
    private final List<Account> accounts;
    private final Map<Long, Long> accountClientMap;

    public Bank(String name) {
        this.name = name;
        accounts = new ArrayList<>();
        clients = new ArrayList<>();
        accountClientMap = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Client> getClients() {
        return clients;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public Map<Long, Long> getAccountClientMap() {
        return accountClientMap;
    }
}
