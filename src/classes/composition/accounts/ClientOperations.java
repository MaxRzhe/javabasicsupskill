package classes.composition.accounts;


public class ClientOperations {
    private final Bank bank;

    public ClientOperations(Bank bank) {
        this.bank = bank;
    }

    public boolean addClient(Client client) {
        if (client != null && !bank.getClients().contains(client)) {
            return bank.getClients().add(client);
        } else {
            return false;
        }
    }

    public long createClient(String firstName, String lastName, String address) {
        Client client = new Client(firstName, lastName, address);
        addClient(client);
        return client.getId();
    }

    public boolean removeClient(long clientId) {
        return bank.getClients().removeIf(client -> client.getId() == clientId);
    }

    public Client findById(long clientId) {
        return bank.getClients().stream()
                .filter(client -> client.getId() == clientId)
                .findFirst()
                .orElse(null);
    }



}
