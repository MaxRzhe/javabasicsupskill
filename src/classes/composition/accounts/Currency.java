package classes.composition.accounts;

public enum Currency {
    DOLLAR("\u0024"),
    EURO("\u20AC"),
    RUSSIAN_RUBLE("\u20BD"),
    POUND_STERLING("\u00A3");

    private final String value;

    Currency(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
