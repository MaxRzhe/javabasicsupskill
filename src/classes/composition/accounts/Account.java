package classes.composition.accounts;

import java.util.Objects;

public class Account {
    private final long id;
    private final long clientId;
    private final Currency currency;
    private double balance;
    private boolean isBlocked;
    private boolean isLoanable;

    public Account(long clientId, Currency currency) {
        this.id = IdGenerator.generate();
        this.balance = 0.0;
        this.isBlocked = false;
        this.isLoanable = false;
        this.clientId = clientId;
        this.currency = currency;
    }

    public long getId() {
        return id;
    }

    public double getBalance() {
        return balance;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public boolean isLoanable() {
        return isLoanable;
    }

    public void setLoanable(boolean loanable) {
        isLoanable = loanable;
    }

    public long getClientId() {
        return clientId;
    }

    public Currency getCurrency() {
        return currency;
    }

    public boolean deposit(double sum) {
        if (sum > 0) {
            if (!isBlocked) {
                balance += sum;
                return true;
            }
        }
        return false;

    }

    public boolean withdraw(double sum) {
        if (!isBlocked) {
            double newBalance = balance - sum;
            if (newBalance < 0 && !isLoanable) {
                return false;
            } else {
                balance = newBalance;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return getId() == account.getId() &&
                getClientId() == account.getClientId() &&
                Double.compare(account.getBalance(), getBalance()) == 0 &&
                isBlocked() == account.isBlocked() &&
                isLoanable() == account.isLoanable() &&
                getCurrency().equals(account.getCurrency());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getClientId(), getBalance(), getCurrency(), isBlocked(), isLoanable());
    }

    @Override
    public String toString() {
        return String.format("******\nAccount\nId: %d\nClientId: %d\nBalance: %.2f\n" +
                        "Currency: %s\nBlocked: %b\nLoanable: %b\n******\n",
                id, clientId, balance, currency.getValue(), isBlocked, isLoanable);
    }
}
