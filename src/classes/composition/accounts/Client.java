package classes.composition.accounts;

import java.util.Objects;

public class Client {
    private final long id;
    private String firstName;
    private String lastName;
    private String address;

    public Client(String firstName, String lastName, String address) {
        this.id = IdGenerator.generate();
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return getId() == client.getId() &&
                getFirstName().equals(client.getFirstName()) &&
                getLastName().equals(client.getLastName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getLastName());
    }

    @Override
    public String toString() {
        return String.format("******\nClient\nId: %d\nFirst Name: %s\nLast Name: %s\nAddress: %s\n******\n",
                id, firstName, lastName, address);
    }
}
