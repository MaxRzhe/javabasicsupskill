package classes.composition.accounts;

public class BankApp {

    public static void main(String[] args) {
        Bank bank = new Bank("Bank of America");
        ClientOperations clientOps = new ClientOperations(bank);
        AccountOperations accountOps = new AccountOperations(bank);

        long clientId = clientOps.createClient("Sergio", "Pollini", "Rome");
        long accountOneId = accountOps.createAccount(clientId, Currency.DOLLAR);
        long accountTwoId = accountOps.createAccount(clientId, Currency.RUSSIAN_RUBLE);
        accountOps.deposit(accountOneId, 3500);

        accountOps.makeLoanable(accountTwoId);
        accountOps.withdraw(accountTwoId, 1200);

        System.out.println(accountOps.totalBalance(clientId));
        System.out.println(accountOps.totalNegativeBalance(clientId));

        accountOps.blockAccount(accountOneId);
        boolean isSuccessful = accountOps.withdraw(accountOneId, 1000);
        System.out.println(isSuccessful);
        System.out.println(accountOps.accountBalance(clientId, accountOneId));

        Account foundAccount = accountOps.findById(accountTwoId);
        System.out.println(foundAccount);
        Client foundClient = clientOps.findById(foundAccount.getClientId());
        System.out.println(foundClient);

    }


}
