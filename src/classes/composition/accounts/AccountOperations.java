package classes.composition.accounts;

public class AccountOperations {
    private final Bank bank;

    public AccountOperations(Bank bank) {
        this.bank = bank;
    }

    public long createAccount(long clientId, Currency currency) {
        Client foundClient = bank.getClients().stream()
                .filter(client -> client.getId() == clientId)
                .findFirst()
                .orElse(null);
        if (foundClient != null) {
            Account account = new Account(clientId, currency);
            bank.getAccounts().add(account);
            bank.getAccountClientMap().putIfAbsent(account.getId(), clientId);
            return account.getId();
        } else {
            return 0;
        }
    }

    public boolean removeAccount(long accountId) {
        return bank.getAccountClientMap().remove(accountId) == accountId &&
                bank.getAccounts().removeIf(account -> account.getId() == accountId);
    }

    public boolean blockAccount(long accountId) {
        Account account = findById(accountId);
        if (account != null) {
            account.setBlocked(true);
            return true;
        } else {
            return false;
        }
    }

    public boolean unblockAccount(long accountId) {
        Account account = findById(accountId);
        if (account != null) {
            account.setBlocked(false);
            return true;
        } else {
            return false;
        }
    }

    public boolean makeLoanable(long accountId) {
        Account account = findById(accountId);
        if (account != null) {
            account.setLoanable(true);
            return true;
        } else {
            return false;
        }
    }

    public boolean makeUnloanable(long accountId) {
        Account account = findById(accountId);
        if (account != null) {
            account.setLoanable(false);
            return true;
        } else {
            return false;
        }
    }

    public boolean deposit(long accountId, double sum) {
        Account account = findById(accountId);
        if (account != null) {
            return account.deposit(sum);
        } else {
            return false;
        }
    }

    public boolean withdraw(long accountId, double sum) {
        Account account = findById(accountId);
        if (account != null) {
            return  account.withdraw(sum);
        } else {
            return false;
        }
    }

    public Account findById(long accountId) {
        return bank.getAccounts().stream()
                .filter(account -> account.getId() == accountId)
                .findFirst()
                .orElse(null);
    }

    public double totalBalance(long clientId) {
        return bank.getAccounts().stream()
                .filter(account -> account.getClientId() == clientId)
                .mapToDouble(Account::getBalance)
                .sum();
    }

    public double totalPositiveBalance(long clientId) {
        return bank.getAccounts().stream()
                .filter(account -> account.getClientId() == clientId && account.getBalance() >= 0)
                .mapToDouble(Account::getBalance)
                .sum();
    }

    public double totalNegativeBalance(long clientId) {
        return bank.getAccounts().stream()
                .filter(account -> account.getClientId() == clientId && account.getBalance() < 0)
                .mapToDouble(Account::getBalance)
                .sum();
    }

    public double accountBalance(long clientId, long accountId) {
        return bank.getAccounts().stream()
                .filter(account -> account.getId() == accountId &&
                        account.getClientId() == clientId)
                .mapToDouble(Account::getBalance)
                .sum();
    }

}
