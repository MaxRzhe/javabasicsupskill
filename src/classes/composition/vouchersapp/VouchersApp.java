package classes.composition.vouchersapp;

import classes.composition.vouchersapp.enums.Accommodation;
import classes.composition.vouchersapp.enums.Country;
import classes.composition.vouchersapp.enums.Transport;
import classes.composition.vouchersapp.vouchers.Voucher;

import java.util.List;

// Туристические путевки. Сформировать набор предложений клиенту по выбору туристической путевки различного типа
// (отдых, экскурсии, лечение, шопинг, круиз и т. д.) для оптимального выбора. Учитывать возможность выбора транспорта,
// питания и числа дней. Реализовать выбор и сортировку путевок.
public class VouchersApp {
    public static void main(String[] args) {
        List<Voucher> offers = DummyData.loadOffers();

        VoucherHandler handler = new VoucherHandler(offers);

        List<Voucher> chinaVouchers = handler.find(Country.CHINA);
        chinaVouchers.forEach(System.out::println);

        List<Voucher> toItaly = handler.find(Country.ITALY, Transport.BUS);
        toItaly.forEach(System.out::println);

        List<Voucher> fullBoardVouchers = handler.find(Accommodation.FULL_BOARD);
        fullBoardVouchers.forEach(System.out::println);

        handler.sortByTotalPrice(chinaVouchers);
        chinaVouchers.forEach(System.out::println);

        handler.sortByDays();
        offers.forEach(System.out::println);
    }
}
