package classes.composition.vouchersapp.enums;

public enum Accommodation {
    ALL_INCLUSIVE,
    FULL_BOARD,
    HALF_BOARD,
    BED_AND_BREAKFAST,
    ROOM_ONLY
}
