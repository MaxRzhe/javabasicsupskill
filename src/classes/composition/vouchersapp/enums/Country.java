package classes.composition.vouchersapp.enums;

public enum Country {
    CHINA,
    ITALY,
    SPAIN,
    CUBA,
    TURKEY;
}
