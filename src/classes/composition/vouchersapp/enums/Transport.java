package classes.composition.vouchersapp.enums;

public enum Transport {
    BUS,
    AIRPLANE,
    SHIP,
    TRAIN
}
