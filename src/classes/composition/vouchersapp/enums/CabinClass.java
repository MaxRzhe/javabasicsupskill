package classes.composition.vouchersapp.enums;

public enum CabinClass {
    ONE(1),
    TWO(2),
    THREE(3);

    private final int cabinClass;

    CabinClass(int cabinClass) {
        this.cabinClass = cabinClass;
    }

    public int getCabinClass() {
        return cabinClass;
    }
}
