package classes.composition.vouchersapp.enums;

public enum Indications {
    CARDIOVASCULAR,
    MUSCULOSKELETAL,
    RESPIRATORY,
    GASTROINTESTINAL
}