package classes.composition.vouchersapp;

import classes.composition.vouchersapp.enums.*;
import classes.composition.vouchersapp.vouchers.*;

import java.util.ArrayList;
import java.util.List;

public abstract class DummyData {

    public static List<Voucher> loadOffers() {
        List<Voucher> offers = new ArrayList<>();
        RelaxVoucher relaxVoucher = new RelaxVoucher("Rome Relaxation", Country.ITALY, Accommodation.HALF_BOARD,
                Transport.AIRPLANE, 10, 500, true);
        offers.add(relaxVoucher);

        RelaxVoucher relaxVoucher2 = new RelaxVoucher("Italian Fairytale", Country.ITALY,
                Accommodation.ALL_INCLUSIVE, Transport.AIRPLANE, 9, 150, false);
        offers.add(relaxVoucher2);

        ShoppingVoucher shoppingVoucher = new ShoppingVoucher("Ankara Shopping", Country.TURKEY,
                Accommodation.BED_AND_BREAKFAST, Transport.BUS, 5);
        shoppingVoucher.increaseLuggageKg(40);
        offers.add(shoppingVoucher);

        ShoppingVoucher shoppingVoucher2 = new ShoppingVoucher("Barcelona Weekend", Country.SPAIN,
                Accommodation.ROOM_ONLY, Transport.AIRPLANE, 4);
        offers.add(shoppingVoucher2);

        TreatmentVoucher treatmentVoucher = new TreatmentVoucher("Mountain Healing", Country.SPAIN,
                Accommodation.FULL_BOARD, Transport.AIRPLANE, 14, Indications.RESPIRATORY, false);
        offers.add(treatmentVoucher);

        TreatmentVoucher treatmentVoucher2 = new TreatmentVoucher("Mystical Lake", Country.CHINA,
                Accommodation.ALL_INCLUSIVE, Transport.AIRPLANE, 10, Indications.CARDIOVASCULAR, true);
        offers.add(treatmentVoucher2);

        CruiseVoucher cruiseVoucher = new CruiseVoucher("Havana Adventure", Country.CUBA,
                Accommodation.FULL_BOARD, 14, CabinClass.TWO);
        cruiseVoucher.increaseLuggageKg(50);
        offers.add(cruiseVoucher);

        CruiseVoucher cruiseVoucher2 = new CruiseVoucher("China Pirates", Country.CHINA,
                Accommodation.BED_AND_BREAKFAST, 20, CabinClass.THREE);
        offers.add(cruiseVoucher2);

        return offers;
    }
}
