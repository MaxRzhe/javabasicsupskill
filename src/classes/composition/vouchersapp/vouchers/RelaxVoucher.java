package classes.composition.vouchersapp.vouchers;

import classes.composition.vouchersapp.enums.Accommodation;
import classes.composition.vouchersapp.enums.Country;
import classes.composition.vouchersapp.enums.Transport;

import java.util.Objects;

public class RelaxVoucher extends Voucher {
    private final int beachDistanceMetre;
    private boolean isExcursionAvailable;

    public RelaxVoucher(String name, Country country,
                        Accommodation accommodation, Transport transport, int days,
                        int beachDistanceMetre, boolean isExcursionAvailable) {
        super(name, country, accommodation, transport, days);
        this.beachDistanceMetre = Math.max(beachDistanceMetre, 0);
        this.isExcursionAvailable = isExcursionAvailable;
    }

    public int getBeachDistanceMetre() {
        return beachDistanceMetre;
    }

    public boolean isExcursionAvailable() {
        return isExcursionAvailable;
    }

    public void setExcursionAvailable(boolean excursionAvailable) {
        isExcursionAvailable = excursionAvailable;
    }

    private double beachCoefficient() {
        if (beachDistanceMetre <= 250) {
            return 1.3;
        } else if (beachDistanceMetre <= 500) {
            return 1.2;
        } else if (beachDistanceMetre <= 1000) {
            return 1.1;
        } else {
            return 0.9;
        }
    }

    @Override
    public double totalPrice() {
        double totalPrice = super.totalPrice();
        return totalPrice * beachCoefficient() + (isExcursionAvailable ? totalPrice * 0.1 : 0.0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RelaxVoucher that = (RelaxVoucher) o;
        return getBeachDistanceMetre() == that.getBeachDistanceMetre() &&
                isExcursionAvailable() == that.isExcursionAvailable();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getBeachDistanceMetre(), isExcursionAvailable());
    }

    @Override
    public String toString() {
        return String.format("******\nRELAX VOUCHER\nName: %s\nCountry: %s\nAccommodation: %s\n" +
                        "Transport: %s\nHow long: %d days\nDistance to the beach: %d m\n" +
                        "Excursions available: %b\nTOTAL PRICE: $ %.2f\n******\n", super.getName(),
                super.getCountry().name(), super.getAccommodation().name(), super.getTransport().name(),
                super.getDays(), beachDistanceMetre, isExcursionAvailable, totalPrice());
    }
}
