package classes.composition.vouchersapp.vouchers;

import classes.composition.vouchersapp.PropertiesReader;
import classes.composition.vouchersapp.enums.Accommodation;
import classes.composition.vouchersapp.enums.Country;
import classes.composition.vouchersapp.enums.Transport;

import java.util.Objects;

public class ShoppingVoucher extends Voucher{
    private final double maxLuggageKg;
    private double currentLuggageKg;

    public ShoppingVoucher(String name, Country country, Accommodation accommodation, Transport transport,
                           int days) {
        super(name, country, accommodation, transport, days);
        this.maxLuggageKg = PropertiesReader.readMaxLuggageKg(country);
        this.currentLuggageKg = maxLuggageKg;
    }

    public double getMaxLuggageKg() {
        return maxLuggageKg;
    }

    public double getCurrentLuggageKg() {
        return currentLuggageKg;
    }

    public void increaseLuggageKg(double kg) {
        if (kg > 0) {
            currentLuggageKg += kg;
        }
    }

    public void removeExtraLuggage() {
        currentLuggageKg = maxLuggageKg;
    }

    @Override
    public double totalPrice() {
        double totalPrice = super.totalPrice();
        double coefficient = 1 + (currentLuggageKg - maxLuggageKg) / maxLuggageKg;
        return totalPrice * coefficient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingVoucher that = (ShoppingVoucher) o;
        return Double.compare(that.getMaxLuggageKg(), getMaxLuggageKg()) == 0 &&
                Double.compare(that.getCurrentLuggageKg(), getCurrentLuggageKg()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMaxLuggageKg(), getCurrentLuggageKg());
    }

    @Override
    public String toString() {
        return String.format("******\nSHOPPING VOUCHER\nName: %s\nCountry: %s\nAccommodation: %s\n" +
                        "Transport: %s\nHow long: %d days\nMax luggage available(kg): %.1f\n" +
                        "Current increased luggage(kg): %.1f\nTOTAL PRICE: $ %.2f\n******\n", super.getName(),
                super.getCountry().name(), super.getAccommodation().name(), super.getTransport().name(),
                super.getDays(), maxLuggageKg, currentLuggageKg, totalPrice());
    }
}
