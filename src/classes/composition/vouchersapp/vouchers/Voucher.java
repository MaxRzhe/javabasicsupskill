package classes.composition.vouchersapp.vouchers;

import classes.composition.vouchersapp.enums.Accommodation;
import classes.composition.vouchersapp.enums.Country;
import classes.composition.vouchersapp.enums.Transport;

import java.util.Objects;

import static classes.composition.vouchersapp.PropertiesReader.readOneDayPrice;
import static classes.composition.vouchersapp.PropertiesReader.readTransportPrice;

public abstract class Voucher {
    private final String name;
    private final Country country;
    private final Accommodation accommodations;
    private final Transport transports;
    private final int days;

    public Voucher(String name, Country country, Accommodation accommodations, Transport transports, int days) {
        this.name = name;
        this.country = country;
        this.accommodations = accommodations;
        this.transports = transports;
        this.days = days;
    }

    public String getName() {
        return name;
    }

    public Country getCountry() {
        return country;
    }

    public Accommodation getAccommodation() {
        return accommodations;
    }

    public Transport getTransport() {
        return transports;
    }

    public int getDays() {
        return days;
    }

    public double totalPrice() {
        double wholeAccommodation = readOneDayPrice(country, accommodations) * days;
        double transportPrice = readTransportPrice(country, transports);
        if (wholeAccommodation != 0.0 && transportPrice != 0.0) {
            return wholeAccommodation + transportPrice;
        } else {
            return 0.0;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Voucher voucher = (Voucher) o;
        return getDays() == voucher.getDays() &&
                getName().equals(voucher.getName()) &&
                getCountry() == voucher.getCountry() &&
                accommodations == voucher.accommodations &&
                transports == voucher.transports;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCountry(), accommodations, transports, getDays());
    }
}
