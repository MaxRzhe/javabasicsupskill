package classes.composition.vouchersapp.vouchers;

import classes.composition.vouchersapp.PropertiesReader;
import classes.composition.vouchersapp.enums.Accommodation;
import classes.composition.vouchersapp.enums.CabinClass;
import classes.composition.vouchersapp.enums.Country;
import classes.composition.vouchersapp.enums.Transport;

import java.util.Objects;

public class CruiseVoucher extends Voucher{
    private CabinClass cabinClass;
    private final double maxLuggageKg;
    private double currentLuggageKg;

    public CruiseVoucher(String name, Country country, Accommodation accommodations, int days, CabinClass cabinClass) {
        super(name, country, accommodations, Transport.SHIP, days);
        this.cabinClass = cabinClass;
        this.maxLuggageKg = PropertiesReader.readMaxLuggageKg(country);
        this.currentLuggageKg = maxLuggageKg;
    }

    public CabinClass getCabinClass() {
        return cabinClass;
    }

    public void changeCabinClass(CabinClass cabinClass) {
        this.cabinClass = cabinClass;
    }

    public double getMaxLuggageKg() {
        return maxLuggageKg;
    }

    public double getCurrentLuggageKg() {
        return currentLuggageKg;
    }

    public void increaseLuggageKg(double kg) {
        if (kg > 0) {
            currentLuggageKg += kg;
        }
    }

    public void removeExtraLuggage() {
        currentLuggageKg = maxLuggageKg;
    }

    private double classCoefficient() {
        if (cabinClass == CabinClass.ONE) {
            return 1.3;
        } else if (cabinClass == CabinClass.TWO) {
            return 1.2;
        } else if (cabinClass == CabinClass.THREE) {
            return 1.1;
        } else {
            return 1.0;
        }
    }

    @Override
    public double totalPrice() {
        double totalPrice = super.totalPrice();
        double luggageCoefficient = 1 + (currentLuggageKg - maxLuggageKg) / maxLuggageKg;
        return totalPrice * classCoefficient() * luggageCoefficient;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CruiseVoucher that = (CruiseVoucher) o;
        return Double.compare(that.getMaxLuggageKg(), getMaxLuggageKg()) == 0 &&
                Double.compare(that.getCurrentLuggageKg(), getCurrentLuggageKg()) == 0 &&
                getCabinClass() == that.getCabinClass();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getCabinClass(), getMaxLuggageKg(), getCurrentLuggageKg());
    }

    @Override
    public String toString() {
        return String.format("******\nCRUISE VOUCHER\nName: %s\nCountry: %s\nAccommodation: %s\n" +
                        "Transport: SHIP\nHow long: %d days\nMax luggage available(kg): %.1f\n" +
                        "Current increased luggage(kg): %.1f\nCabin class: %s\nTOTAL PRICE: $ %.1f\n******\n", super.getName(),
                super.getCountry().name(), super.getAccommodation().name(), super.getDays(),
                maxLuggageKg, currentLuggageKg, cabinClass.getCabinClass(), totalPrice());
    }
}
