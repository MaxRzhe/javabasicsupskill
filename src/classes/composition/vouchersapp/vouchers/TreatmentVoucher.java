package classes.composition.vouchersapp.vouchers;

import classes.composition.vouchersapp.enums.Accommodation;
import classes.composition.vouchersapp.enums.Country;
import classes.composition.vouchersapp.enums.Indications;
import classes.composition.vouchersapp.enums.Transport;

import java.util.Objects;


public class TreatmentVoucher extends Voucher {
    private Indications indications;
    private boolean isExcursionAvailable;

    public TreatmentVoucher(String name, Country country,
                            Accommodation accommodation, Transport transport, int days,
                            Indications indications, boolean isExcursionAvailable) {
        super(name, country, accommodation, transport, days);
        this.indications = indications;
        this.isExcursionAvailable = isExcursionAvailable;
    }


    public Indications getIndications() {
        return indications;
    }

    public void setIndications(Indications indications) {
        this.indications = indications;
    }

    public boolean isExcursionAvailable() {
        return isExcursionAvailable;
    }

    public void setExcursionAvailable(boolean excursionAvailable) {
        isExcursionAvailable = excursionAvailable;
    }

    private double indicationsCoefficient() {
        if (indications == Indications.RESPIRATORY) {
            return 1.2;
        } else if (indications == Indications.CARDIOVASCULAR) {
            return 1.3;
        } else if (indications == Indications.GASTROINTESTINAL) {
            return 0.9;
        } else if (indications == Indications.MUSCULOSKELETAL) {
            return 1.1;
        } else {
            return 1.0;
        }
    }

    @Override
    public double totalPrice() {
        double totalPrice = super.totalPrice();
        return totalPrice * indicationsCoefficient() + (isExcursionAvailable ? totalPrice * 0.1 : 0.0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreatmentVoucher that = (TreatmentVoucher) o;
        return isExcursionAvailable() == that.isExcursionAvailable() &&
                getIndications() == that.getIndications();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIndications(), isExcursionAvailable());
    }

    @Override
    public String toString() {
        return String.format("******\nTREATMENT VOUCHER\nName: %s\nCountry: %s\nAccommodation: %s\n" +
                        "Transport: %s\nHow long: %d days\nIndications: %s\nExcursions: %b\nTOTAL PRICE: $ %.2f\n******\n", super.getName(),
                super.getCountry().name(), super.getAccommodation().name(), super.getTransport().name(), super.getDays(),
                indications.name(), isExcursionAvailable, totalPrice());
    }
}
