package classes.composition.vouchersapp;

import classes.composition.vouchersapp.enums.Accommodation;
import classes.composition.vouchersapp.enums.Country;
import classes.composition.vouchersapp.enums.Transport;
import classes.composition.vouchersapp.vouchers.Voucher;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class VoucherHandler {
    private final List<Voucher> vouchers;

    public VoucherHandler(List<Voucher> vouchers) {
        this.vouchers = vouchers;
    }

    public List<Voucher> find(Country country) {
        return vouchers.stream()
                .filter(voucher -> voucher.getCountry() == country)
                .collect(Collectors.toList());
    }

    public List<Voucher> find(Transport transport) {
        return vouchers.stream()
                .filter(voucher -> voucher.getTransport() == transport)
                .collect(Collectors.toList());
    }

    public List<Voucher> find(Accommodation accommodation) {
        return vouchers.stream()
                .filter(voucher -> voucher.getAccommodation() == accommodation)
                .collect(Collectors.toList());
    }

    public List<Voucher> find(Country country, Transport transport) {
        return vouchers.stream()
                .filter(voucher -> voucher.getCountry() == country && voucher.getTransport() == transport)
                .collect(Collectors.toList());
    }

    public List<Voucher> find(Country country, Accommodation accommodation) {
        return vouchers.stream()
                .filter(voucher -> voucher.getCountry() == country && voucher.getAccommodation() == accommodation)
                .collect(Collectors.toList());
    }

    public List<Voucher> find(Country country, Accommodation accommodation, Transport transport) {
        return vouchers.stream()
                .filter(voucher -> voucher.getCountry() == country &&
                        voucher.getTransport() == transport &&
                        voucher.getAccommodation() == accommodation)
                .collect(Collectors.toList());
    }

    public void sortByTotalPrice() {
        vouchers.sort(Comparator.comparingDouble(Voucher::totalPrice));
    }

    public void sortByTotalPrice(List<Voucher> voucherList) {
        voucherList.sort(Comparator.comparingDouble(Voucher::totalPrice));
    }

    public void sortByDays() {
        vouchers.sort(Comparator.comparingInt(Voucher::getDays));
    }

    public void sortByDays(List<Voucher> voucherList) {
        voucherList.sort(Comparator.comparingInt(Voucher::getDays));
    }

}
