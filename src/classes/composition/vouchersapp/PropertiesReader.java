package classes.composition.vouchersapp;

import classes.composition.vouchersapp.enums.Accommodation;
import classes.composition.vouchersapp.enums.Country;
import classes.composition.vouchersapp.enums.Transport;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {
    private static final Properties properties = new Properties();
    private static final String propertiesFile = "app_offers.properties";

    public static double readOneDayPrice(Country country, Accommodation accommodation) {
        String prop = readConfig(String.format("%s_%s", accommodation.name(), country.name()));
        if (!prop.isEmpty()) {
            try {
                return Double.parseDouble(prop);
            } catch (NumberFormatException e) {
                System.out.println("Invalid property value");
                return 0.0;
            }
        }
        return 0.0;
    }

    public static double readTransportPrice(Country country, Transport transport) {
        String prop = readConfig(String.format("%s_%s", transport.name(), country.name()));
        if (!prop.isEmpty()) {
            try {
                return Double.parseDouble(prop);
            } catch (NumberFormatException e) {
                System.out.println("Invalid property value");
                return 0.0;
            }
        }
        return 0.0;

    }

    public static double readMaxLuggageKg(Country country) {
        String prop = readConfig(String.format("LUGGAGE_%s", country.name()));
        if (!prop.isEmpty()) {
            try {
                return Double.parseDouble(prop);
            } catch (NumberFormatException e) {
                System.out.println("Invalid property value");
                return 0.0;
            }
        }
        return 0.0;
    }



    private static String readConfig(String name) {
        String packagePath = PropertiesReader.class
                .getPackageName()
                .replaceAll("\\.", "\\\\");

        InputStream configStream = PropertiesReader.class
                .getClassLoader()
                .getResourceAsStream(String.format("%s\\%s", packagePath, propertiesFile));

        if (configStream != null) {
            try {
                properties.load(configStream);
                return properties.getProperty(String.format("%s", name));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Failed to read properties file");
        }
        return "";
    }
}
