package classes.simplestclasses.triangle;

public class Runner {
    public static void main(String[] args) {
        Point pointA = new Point(5, 3);
        Point pointB = new Point(8, 15);
        Point pointC = new Point(-6, -3);

        Triangle triangle = new Triangle(pointA, pointB, pointC);
        System.out.println("Сторона AB: " + triangle.getSideAB());
        System.out.println("Сторона BC: " + triangle.getSideBC());
        System.out.println("Сторона CA: " + triangle.getSideCA());
        System.out.println();
        System.out.println("Медиана A: " + triangle.getMedianA());
        System.out.println("Медиана B: " + triangle.getMedianB());
        System.out.println("Медиана C: " + triangle.getMedianC());
        System.out.println();
        System.out.println("Периметр: " + triangle.perimeter());
        System.out.println("Площадь: " + triangle.area());
        System.out.println("Точка пересечения медиан: " + triangle.getMedianIntersectionPoint());

    }
}
