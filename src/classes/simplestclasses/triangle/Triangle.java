package classes.simplestclasses.triangle;

//Описать класс, представляющий треугольник.
//Предусмотреть методы для создания объектов, вычисления площади, периметра и точки пересечения медиан.
public class Triangle {
    private final Line sideAB;
    private final Line sideBC;
    private final Line sideCA;

    private final Line medianA;
    private final Line medianB;
    private final Line medianC;

    public Triangle(Point apexA, Point apexB, Point apexC) {
        if ((apexA.getX() == apexB.getX() && apexB.getX() == apexC.getX()) ||
                (apexA.getY() == apexB.getY() && apexB.getY() == apexC.getY())) {
            throw new IllegalArgumentException("Невозможно построить треугольник по заданным координатам");
        }
        this.sideAB = new Line(apexA, apexB);
        this.sideBC = new Line(apexB, apexC);
        this.sideCA = new Line(apexC, apexA);
        this.medianA = getMedian(apexA, sideBC);
        this.medianB = getMedian(apexB, sideCA);
        this.medianC = getMedian(apexC, sideAB);
    }

    public Line getSideAB() {
        return sideAB;
    }

    public Line getSideBC() {
        return sideBC;
    }

    public Line getSideCA() {
        return sideCA;
    }

    public Line getMedianA() {
        return medianA;
    }

    public Line getMedianB() {
        return medianB;
    }

    public Line getMedianC() {
        return medianC;
    }

    public double perimeter() {
        return getSideAB().length() +
                getSideBC().length() +
                getSideCA().length();
    }

    public double area() {
        double halfPerimeter = perimeter() / 2;
        return Math.sqrt(halfPerimeter *
                (halfPerimeter - getSideAB().length()) *
                (halfPerimeter - getSideBC().length()) *
                (halfPerimeter - getSideCA().length()));
    }

    private Line getMedian(Point apexPoint, Line side) {
        Point start = side.getPointOne();
        Point end = side.getPointTwo();
        Point middle = new Point((start.getX() + end.getX()) / 2, (start.getY() + end.getY()) / 2);
        return new Line(apexPoint, middle);
    }

    public Point getMedianIntersectionPoint() {
        Point apexMedianPoint = getMedianA().getPointOne();
        Point endMedian = getMedianA().getPointTwo();
        double x = (apexMedianPoint.getX() + 2 * endMedian.getX()) / 3;
        double y = (apexMedianPoint.getY() + 2 * endMedian.getY()) / 3;
        return new Point(x, y);
    }


}
