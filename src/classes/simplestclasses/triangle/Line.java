package classes.simplestclasses.triangle;

public class Line {
    private final Point pointOne;
    private final Point pointTwo;

    public Line(Point pointOne, Point pointTwo) {
        this.pointOne = pointOne;
        this.pointTwo = pointTwo;
    }

    public Point getPointOne() {
        return pointOne;
    }

    public Point getPointTwo() {
        return pointTwo;
    }

    public double length() {
        return Math.sqrt(Math.pow(getPointTwo().getX() - getPointOne().getX(), 2) +
                Math.pow(getPointTwo().getY() - getPointOne().getY(), 2));
    }

    @Override
    public String toString() {
        return pointOne +
                ", " + pointTwo +
                ", length=" + length();
    }
}

