package classes.simplestclasses;


//Создайте класс Test1 с двумя переменными. Добавьте метод вывода на экран и методы изменения этих переменных.
// Добавьте метод, который находит сумму значений этих переменных, и метод,
// который находит наибольшее значение из этих двух переменных.
public class Test1 {
    private int x;
    private int y;

    public Test1(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void printX() {
        System.out.println(x);
    }

    public void printY() {
        System.out.println(y);
    }

    public int getSum(int x, int y) {
        return x + y;
    }

    public int getMax(int x, int y) {
        return Math.max(x, y);
    }

}
