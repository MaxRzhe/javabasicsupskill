package classes.simplestclasses;

//Составьте описание класса для представления времени.
// Предусмотрте возможности установки времени и изменения его отдельных полей (час, минута, секунда)
// с проверкой допустимости вводимых значений. В случае недопустимых значений полей поле устанавливается в значение 0.
// Создать методы изменения времени на заданное количество часов, минут и секунд.
public class TimeHandler {
    private int hours;
    private int minutes;
    private int seconds;

    public TimeHandler() {
        this.hours = 0;
        this.minutes = 0;
        this.seconds = 0;
    }

    public TimeHandler(int hours, int minutes, int seconds) {
        this.hours = hours < 0 || hours > 23 ? 0 : hours;
        this.minutes = minutes < 0 || minutes > 59 ? 0 : minutes;
        this.seconds = seconds < 0 || seconds > 59 ? 0 : seconds;
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void changeHours(int hours) {
        change(hours * 3600);
    }

    public void changeMinutes(int minutes) {
        change(minutes * 60);
    }

    public void changeSeconds(int seconds) {
        change(seconds);
    }

    public void change(int seconds) {
        int[] timeArray = parseSeconds(currentTimeInSeconds() + seconds);
        this.seconds = timeArray[2];
        this.minutes = timeArray[1];
        this.hours = timeArray[0];
        if (this.hours >= 24) {
            this.hours %= 24;
        }
    }

    private int[] parseSeconds(int seconds) {
        int[] time = new int[3];
        int hours = seconds / 3600;
        int minutes = seconds % 3600 / 60;
        int sec = seconds % 3600 % 60;
        time[0] = hours;
        time[1] = minutes;
        time[2] = sec;
        return time;
    }

    private int currentTimeInSeconds() {
        return this.hours * 3600 + this.minutes * 60 + this.seconds;
    }

    @Override
    public String toString() {
        return "TimeHandler{" +
                "hours=" + hours +
                ", minutes=" + minutes +
                ", seconds=" + seconds +
                '}';
    }

    public static void main(String[] args) {
        TimeHandler handler = new TimeHandler(13, 26, 58);


        handler.changeSeconds(96);
        System.out.println(handler);

        handler.changeMinutes(-26);
        System.out.println(handler);

        handler.changeHours(23);
        System.out.println(handler);
    }

}
