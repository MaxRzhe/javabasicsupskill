package classes.simplestclasses.customer;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Bank {
    private List<Customer> customers;

    public Bank(List<Customer> customers) {
        this.customers = customers;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public boolean addCustomer(Customer customer) {
        if (customers.contains(customer)) {
            return false;
        }
        return customers.add(customer);
    }

    public boolean deleteCustomer(Customer customer) {
        return customers.remove(customer);
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public List<Customer> sortByLastName() {
        return this.customers.stream()
                .sorted(Comparator.comparing(Customer::getLastName)).collect(Collectors.toList());
    }

    public List<Customer> findByCardNumber(int min, int max) {
        return this.customers.stream()
                .filter(c -> c.getCardNumber() >= min && c.getCardNumber() <= max)
                .collect(Collectors.toList());
    }


}
