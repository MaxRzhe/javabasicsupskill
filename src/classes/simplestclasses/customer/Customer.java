package classes.simplestclasses.customer;

import java.util.Objects;

public class Customer {
    private int id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String address;
    private int cardNumber;
    private String account;

    public Customer(int id, String firstName, String middleName, String lastName, String address, int cardNumber, String account) {
        this.id = id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.address = address;
        this.cardNumber = cardNumber;
        this.account = account;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return getId() == customer.getId() &&
                getCardNumber() == customer.getCardNumber() &&
                getFirstName().equals(customer.getFirstName()) &&
                getMiddleName().equals(customer.getMiddleName()) &&
                getLastName().equals(customer.getLastName()) &&
                getAddress().equals(customer.getAddress()) &&
                getAccount().equals(customer.getAccount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getMiddleName(), getLastName(), getAddress(), getCardNumber(), getAccount());
    }

    @Override
    public String toString() {
        return String.format("Customer -> %s %c.%c. Id: %d. Address: %s. CardNumber: %d. Account: %s",
                this.getLastName(), this.getFirstName().charAt(0), this.getMiddleName().charAt(0), this.getId(),
                this.getAddress(), this.getCardNumber(), this.getAccount());
    }
}
