package classes.simplestclasses.customer;

import java.util.ArrayList;
import java.util.List;

//Создать класс Customer, спецификация которого приведена ниже. Определить конструкторы, set- и get- методы и метод  toString().
// Создать второй класс, агрегирующий массив типа Customer, с подходящими конструкторами и методами.
// Задать критерии выбора данных и вывести эти данные на консоль.
//Класс Customer: id, фамилия, имя, отчество, адрес, номер кредитной карточки, номер банковского счета.
//Найти и вывести:
//a) список покупателей в алфавитном порядке;
//b) список покупателей, у которых номер кредитной карточки находится в заданном интервале
public class BankApp {

    public static List<Customer> customers = new ArrayList<>();
    static {
        Customer customer1 = new Customer(1, "Mike", "Johns",
                "Johnson", "New-York", 1234, "BSSF1763");
        customers.add(customer1);
        Customer customer2 = new Customer(2, "Poul", "Cristof",
                "Smith", "London", 3654, "BDSG4376");
        customers.add(customer2);
        Customer customer3 = new Customer(3, "Juter", "Kilam",
                "Kuber", "Milan", 2021, "BDWA8745");
        customers.add(customer3);
        Customer customer4 = new Customer(4, "Louse", "Lipod",
                "Joter", "Paris", 3698, "GTSF3245");
        customers.add(customer4);
        Customer customer5 = new Customer(5, "Karl", "Parl",
                "Madchen", "Berlin", 5478, "ASER0909");
        customers.add(customer5);
        Customer customer6 = new Customer(6, "Peter", "Koter",
                "Kilov", "London", 6301, "HJUY2314");
        customers.add(customer6);
        Customer customer7 = new Customer(7, "Hans", "Sutea",
                "Blinov", "Berlin", 4196, "QWER4563");
        customers.add(customer7);
        Customer customer8 = new Customer(8, "Liam", "Nicky",
                "Vicino", "Rome", 7421, "GHYF0987");
        customers.add(customer8);
        Customer customer9 = new Customer(9, "Pipe", "Pilac",
                "Couper", "Madrid", 3625, "VBHG2156");
        customers.add(customer9);
        Customer customer10 = new Customer(10, "Mucho", "Viche",
                "Morgan", "Barcelona", 2741, "SWAQ4356");
        customers.add(customer10);
    }

    public static void main(String[] args) {
        Bank bank = new Bank(customers);
        List<Customer> sortedCustomers = bank.sortByLastName();
        sortedCustomers.forEach(System.out::println);

        System.out.println();

        List<Customer> foundCustomers = bank.findByCardNumber(2000, 4000);
        foundCustomers.forEach(System.out::println);
    }
}
