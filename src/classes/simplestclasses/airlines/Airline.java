package classes.simplestclasses.airlines;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;

public class Airline {
    private String destination;
    private String flightNumber;
    private AirplaneType airplaneType;
    private LocalTime arrivalTime;
    private List<DayOfWeek> daysOfWeek;

    public Airline(String destination, String flightNumber, AirplaneType airplaneType, LocalTime arrivalTime, List<DayOfWeek> daysOfWeek) {
        this.destination = destination;
        this.flightNumber = flightNumber;
        this.airplaneType = airplaneType;
        this.arrivalTime = arrivalTime;
        this.daysOfWeek = daysOfWeek;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public AirplaneType getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(AirplaneType airplaneType) {
        this.airplaneType = airplaneType;
    }

    public LocalTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public List<DayOfWeek> getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(List<DayOfWeek> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Airline airline = (Airline) o;
        return getDestination().equals(airline.getDestination()) &&
                getFlightNumber().equals(airline.getFlightNumber()) &&
                getAirplaneType() == airline.getAirplaneType() &&
                getArrivalTime().equals(airline.getArrivalTime()) &&
                getDaysOfWeek().equals(airline.getDaysOfWeek());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDestination(), getFlightNumber(), getAirplaneType(), getArrivalTime(), getDaysOfWeek());
    }

    @Override
    public String toString() {
        return "Airline{" +
                "destination='" + destination + '\'' +
                ", flightNumber='" + flightNumber + '\'' +
                ", airplaneType=" + airplaneType +
                ", arrivalTime=" + arrivalTime +
                ", daysOfWeek=" + daysOfWeek +
                '}';
    }
}
