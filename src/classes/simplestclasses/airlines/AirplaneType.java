package classes.simplestclasses.airlines;

enum AirplaneType {
    PASSENGER, CARGO, PRIVATE
}
