package classes.simplestclasses.airlines;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

// Создать класс Airline, спецификация которого приведена ниже. Определить конструкторы, set- и get- методы и метод toString().
// Создать второй класс, агрегирующий массив типа Airline, с подходящими конструкторами и методами.
// Задать критерии выбора данных и вывести эти данные на консоль.
// Airline: пункт назначения, номер рейса, тип самолета, время вылета, дни недели.
//Найти и вывести:
//a) список рейсов для заданного пункта назначения;
//b) список рейсов для заданного дня недели;
//c) список рейсов для заданного дня недели, время вылета для которых больше заданного.
public class AirlinesApp {
    public static List<Airline> airlines = new ArrayList<>();

    static {
        Airline airline1 = new Airline("Milan", "MISD1475",
                AirplaneType.PASSENGER, LocalTime.of(10, 25),
                List.of(DayOfWeek.MONDAY, DayOfWeek.WEDNESDAY, DayOfWeek.FRIDAY));
        airlines.add(airline1);
        Airline airline2 = new Airline("Paris", "PRSI4578",
                AirplaneType.PASSENGER, LocalTime.of(16, 30),
                List.of(DayOfWeek.THURSDAY, DayOfWeek.SATURDAY));
        airlines.add(airline2);
        Airline airline3 = new Airline("London", "LNDN4896",
                AirplaneType.PRIVATE, LocalTime.of(3, 50),
                List.of(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.SUNDAY));
        airlines.add(airline3);
        Airline airline4 = new Airline("Milan", "MICG8745",
                AirplaneType.CARGO, LocalTime.of(7, 0),
                List.of(DayOfWeek.FRIDAY, DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));
        airlines.add(airline4);
        Airline airline5 = new Airline("London", "LNPS0054",
                AirplaneType.PASSENGER, LocalTime.of(14, 10),
                List.of(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.FRIDAY));
        airlines.add(airline5);
        Airline airline6 = new Airline("Barcelona", "BRSL7410",
                AirplaneType.PRIVATE, LocalTime.of(19, 45),
                List.of(DayOfWeek.FRIDAY));
        airlines.add(airline6);

    }

    public static void main(String[] args) {
        Airport airport = new Airport(airlines);

        List<Airline> toMilanFlights = airport.findByDestination("Milan");
        toMilanFlights.forEach(System.out::println);
        System.out.println();

        List<Airline> onSaturdayFlights = airport.findByDayOfWeek(DayOfWeek.SUNDAY);
        onSaturdayFlights.forEach(System.out::println);
        System.out.println();

        List<Airline> onMondayAfterEight = airport.findByDayOfWeekTimeAfter(DayOfWeek.MONDAY, LocalTime.of(8, 0));
        onMondayAfterEight.forEach(System.out::println);
    }
}
