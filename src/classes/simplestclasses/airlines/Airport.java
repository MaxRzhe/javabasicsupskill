package classes.simplestclasses.airlines;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

public class Airport {
    private List<Airline> airlines;

    public Airport(List<Airline> airlines) {
        this.airlines = airlines;
    }

    public List<Airline> getAirlines() {
        return airlines;
    }

    public void setAirlines(List<Airline> airlines) {
        this.airlines = airlines;
    }

    public boolean addAirline(Airline airline) {
        if (airlines.contains(airline)) {
            return false;
        } else {
            return airlines.add(airline);
        }
    }

    public boolean deleteAirline(Airline airline) {
        return airlines.remove(airline);
    }

    public List<Airline> findByDestination(String destination) {
        return getAirlines().stream()
                .filter(airline -> airline.getDestination().equals(destination))
                .collect(Collectors.toList());
    }

    public List<Airline> findByDayOfWeek(DayOfWeek dayOfWeek) {
        return getAirlines().stream()
                .filter(airline -> contains(airline.getDaysOfWeek(), dayOfWeek))
                .collect(Collectors.toList());
    }

    public List<Airline> findByDayOfWeekTimeAfter(DayOfWeek dayOfWeek, LocalTime arrivalTime) {
        return getAirlines().stream()
                .filter(airline -> contains(airline.getDaysOfWeek(), dayOfWeek) &&
                        airline.getArrivalTime().isAfter(arrivalTime))
                .collect(Collectors.toList());
    }

    private boolean contains(List<DayOfWeek> days, DayOfWeek dayOfWeek) {
        return days.stream().anyMatch(day -> day.equals(dayOfWeek));
    }
}
