package classes.simplestclasses;

//Опишите класс, реализующий десятичный счетчик, который может увеличивать или уменьшать свое значение на единицу в заданном диапазоне.
// Предусмотрите инициализацию счетчика значениями по умолчанию и произвольными значениями.
// Счетчик имеет методы увеличения и уменьшения состояния, и метод позволяющее получить его текущее состояние.
// Написать код, демонстрирующий все возможности класса.
public class Counter {
    private final int start;
    private final int finish;
    private int value;

    public Counter() {
        this.start = 0;
        this.finish = 10;
        this.value = 0;
    }

    public Counter(final int start, final int finish) {
        this.start = start;
        this.value = start;
        this.finish = finish;
    }


    public int getValue() {
        return this.value;
    }

    public int getFinish() {
        return this.finish;
    }

    public int getStart() {
        return start;
    }

    public void increment() {
        if (value + 1 > finish) {
            System.out.println("You have reached the maximum value. You can reset the counter.");
        } else {
            value++;
        }
    }

    public void decrement() {
        if (value - 1 < start) {
            System.out.println("You have reached the maximum value.");
        } else {
            value--;
        }
    }

    public void reset() {
        this.value = 0;
    }


    public static void main(String[] args) {
        Counter counterOne = new Counter(10, 20);
        for (int i = counterOne.getStart(); i < counterOne.getFinish(); i++) {
            counterOne.increment();
            System.out.printf("Current value: %d\n", counterOne.getValue());
        }

        System.out.printf("Current value: %d\n", counterOne.getValue());
        counterOne.increment();

        counterOne.reset();
        System.out.printf("Current value: %d\n", counterOne.getValue());


        Counter counterTwo = new Counter();
        for (int i = 0; i < counterTwo.getFinish() - 5; i++) {
            counterTwo.increment();
        }
        System.out.printf("Current value: %d\n", counterTwo.getValue());

        counterTwo.decrement();
        System.out.printf("Current value: %d\n", counterTwo.getValue());

        counterTwo.reset();
        System.out.printf("Current value: %d\n", counterTwo.getValue());


    }
}
