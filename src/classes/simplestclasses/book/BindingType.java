package classes.simplestclasses.book;

enum BindingType {
    HARD, SOFT, INTEGRAL, ADHESIVE_SEWING, METAL_SPRING, BOLT_FIXING
}
