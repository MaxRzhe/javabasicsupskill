package classes.simplestclasses.book;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class Library {
    private final List<Book> books;

    public Library(List<Book> books) {
        this.books = books;
    }

    public List<Book> getBooks() {
        return books;
    }

    public boolean addBook(Book book) {
        if (books.contains(book)) {
            return false;
        } else {
            return books.add(book);
        }
    }

    public boolean deleteBook(Book book) {
        return books.remove(book);
    }

    public List<Book> findByAuthor(String author) {
        return getBooks().stream()
                .filter(book -> book.getAuthor().equals(author))
                .collect(Collectors.toList());
    }

    public List<Book> findByPublished(String publishedBy) {
        return getBooks().stream()
                .filter(book -> book.getPublishedBy().equals(publishedBy))
                .collect(Collectors.toList());
    }

    public List<Book> findByYearAfter(LocalDate year) {
        return getBooks().stream()
                .filter(book -> book.getYear().isAfter(year))
                .collect(Collectors.toList());
    }
}
