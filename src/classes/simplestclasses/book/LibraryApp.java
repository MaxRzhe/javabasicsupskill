package classes.simplestclasses.book;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

//Создать класс Book, спецификация которого приведена ниже. Определить конструкторы, set- и get- методы и метод  toString().
//Создать второй класс, агрегирующий массив типа Book, с подходящими конструкторами и методами.
//Задать критерии выбора данных и вывести эти данные на консоль.
//Book: id, название, автор(ы), издательство, год издания, количество страниц, цена, тип переплета.
//Найти и вывести:
//a) список книг заданного автора;
//b) список книг, выпущенных заданным издательством;
//c) список книг, выпущенных после заданного года.
public class LibraryApp {

    public static List<Book> books = new ArrayList<>();

    static {
        Book book = new Book(1, "Some title", "Smith M.", "New-York",
                LocalDate.of(1856, 1, 1), 236, 19.99, BindingType.HARD);
        books.add(book);
        Book book1 = new Book(2, "Still some title", "Poulter G.", "London",
                LocalDate.of(1836, 1, 1), 365, 29.99, BindingType.SOFT);
        books.add(book1);
        Book book2 = new Book(3, "One title more", "Twins V.", "Milan",
                LocalDate.of(1993, 1, 1), 132, 39.99, BindingType.METAL_SPRING);
        books.add(book2);
        Book book3 = new Book(4, "Book about nothing", "Garret L.", "London",
                LocalDate.of(1951, 1, 1), 59, 24.99, BindingType.HARD);
        books.add(book3);
        Book book4 = new Book(5, "Interesting book", "Priston N.", "Milan",
                LocalDate.of(1911, 1, 1), 210, 9.99, BindingType.ADHESIVE_SEWING);
        books.add(book4);
        Book book5 = new Book(6, "Don't read this book", "Kallec C.", "Rome",
                LocalDate.of(1896, 1, 1), 390, 16.99, BindingType.SOFT);
        books.add(book5);
        Book book6 = new Book(7, "Funny book", "Ustov B.", "New-York",
                LocalDate.of(1969, 1, 1), 148, 32.99, BindingType.HARD);
        books.add(book6);
        Book book7 = new Book(8, "How to create the book title?", "Licky S.", "New-York",
                LocalDate.of(1984, 1, 1), 320, 21.99, BindingType.HARD);
        books.add(book7);
        Book book8 = new Book(9, "What to do?", "Swerget A.", "Paris",
                LocalDate.of(2003, 1, 1), 124, 29.99, BindingType.BOLT_FIXING);
        books.add(book8);
        Book book9 = new Book(10, "Head first New Year", "Klaus S.", "Moscow",
                LocalDate.of(1955, 1, 1), 136, 19.99, BindingType.SOFT);
        books.add(book9);
    }

    public static void main(String[] args) {

        Library library = new Library(books);
        List<Book> byAuthor = library.findByAuthor("Garret L.");
        byAuthor.forEach(System.out::println);
        System.out.println();

        List<Book> byPublished = library.findByPublished("New-York");
        byPublished.forEach(System.out::println);
        System.out.println();

        List<Book> byYearAfter = library.findByYearAfter(LocalDate.of(1900, 1, 1));
        byYearAfter.forEach(System.out::println);

    }

}
