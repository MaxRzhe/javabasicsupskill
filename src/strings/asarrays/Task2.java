package strings.asarrays;

public class Task2 {

    //Замените в строке все вхождения 'word' на 'letter'.
    public static String changeWord(String line) {
        return line.replaceAll("word", "letter");
    }
}
