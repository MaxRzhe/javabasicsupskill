package strings.asarrays;

import java.util.ArrayList;
import java.util.List;

public class Task1 {

    //Дан массив названий переменных в camelCase. Преобразовать названия в snake_case.
    //camelCase to snake_case
    //первфй вариант
    public static String[] camelToSnakeCase(String[] names) {
        List<String> result = new ArrayList<>();
        for (String name : names) {
            String[] letters = name.split("");
            for (int i = 0; i < letters.length; i++) {
                char ch = letters[i].charAt(0);
                if (Character.isUpperCase(ch)) {
                    letters[i] = "_" + Character.toLowerCase(ch);
                }
            }
            result.add(String.join("", letters));
        }
        return result.toArray(new String[0]);
    }

    //второй вариант
    public static String[] camelCaseToSnakeCase(String[] fieldNamesArray) {
        List<String> result = new ArrayList<>();
        for (String s : fieldNamesArray) {
            s = s.replaceAll("([A-Z][a-z])", "_$1").toLowerCase();
            result.add(s);
        }
        return result.toArray(new String[0]);
    }
}
