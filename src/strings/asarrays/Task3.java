package strings.asarrays;

import java.util.Arrays;

public class Task3 {

    //В строке найти количество цифр
    public static int digitsCount(String letter) {
        return (int) Arrays.stream(letter.split(""))
                .filter(Task3::isDigit)
                .count();
    }

    private static boolean isDigit(String letter) {
        return Character.isDigit(letter.charAt(0));
    }
}
