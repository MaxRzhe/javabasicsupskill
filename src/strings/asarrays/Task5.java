package strings.asarrays;

import java.util.ArrayList;
import java.util.List;

public class Task5 {

    //Удалить в строке все лишние пробелы, то есть серии подряд идущих пробелов заменить на одиночные пробелы.
    //Крайние пробелы в строке удалить.

    //первый вариант
    public static String removeExtraGaps(String line) {
        return line.trim().replaceAll("\\s+", " ");
    }


    //второй вариант
    public static String removeGaps(String line) {
        int gapsCount = 0;
        List<String> modifiedLine = new ArrayList<>();
        String[] letters = line.split("");
        for (String s : letters) {
            gapsCount = s.equals(" ") ? ++gapsCount : 0;
            if (gapsCount < 2) {
                modifiedLine.add(s);
            }
        }
        if (modifiedLine.get(0).equals(" "))
            modifiedLine.remove(0);

        if (modifiedLine.get(modifiedLine.size() - 1).equals(" "))
            modifiedLine.remove(modifiedLine.size() - 1);

        return String.join("", modifiedLine);
    }
}
