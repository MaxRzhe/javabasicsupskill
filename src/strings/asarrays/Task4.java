package strings.asarrays;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task4 {

    //В строке найти количество чисел.

    //первый вариант
    public static int numCount(String line) {
        int count = 0;
        Pattern pattern = Pattern.compile("\\d+(\\.\\d+)?");
        Matcher matcher = pattern.matcher(line);
        while (matcher.find()) {
            count++;
        }
        return count;
    }

    //второй вариант
    public static int countNums(String line) {
        int count = 0;
        int pos = 0;
        List<Character> digits = new ArrayList<>();
        for (char c : line.toCharArray()) {
            if (Character.isDigit(c)) {
                digits.add(c);
                if (pos == line.length() - 1) {
                    count++;
                    digits.clear();
                }
            } else if (c == '.' && !digits.isEmpty() && !digits.contains('.') && pos != line.length() - 1) {
                digits.add(c);
            } else {
                if (!digits.isEmpty()) {
                    count++;
                    digits.clear();
                }
            }
            pos++;
        }
        return count;
    }
}
