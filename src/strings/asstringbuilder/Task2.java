package strings.asstringbuilder;

public class Task2 {

    //В строке вставить после каждого символа 'a' символ 'b'.
    //первый вариант
    public static String insertB(String text) {
        StringBuilder builder = new StringBuilder();
        for (String s : text.split("")) {
            if (s.equals("a")) {
                builder.append("ab");
            } else {
                builder.append(s);
            }
        }
        return builder.toString();
    }

    //второй вариант
    public static String putAfterA(String text) {
        return text.replaceAll("a", "ab");
    }
}
