package strings.asstringbuilder;

public class Task4 {

    //С помощью функции копирования и операции конкатенации составить из частей слова “информатика” слово “торт”.
    public static void modifyWord() {
        String info  = "информатика";
        String t = info.substring(7, 8);
        String o = info.substring(3, 4);
        String p = info.substring(4, 5);
        System.out.println(t.concat(o).concat(p).concat(t));
    }
}
