package strings.asstringbuilder;

import java.util.Arrays;

public class Task1 {

    //Дан текст (строка). Найдите наибольшее количество подряд идущих пробелов в нем
    //первый вариант
    public static int maxGaps(String text) {
        int max = 0;
        StringBuilder builder = new StringBuilder();
        for (String s : text.split("")) {
            if (s.equals(" ")) {
                builder.append(s);
            } else {
                max = Math.max(max, builder.length());
                builder.setLength(0);
            }
        }
        return max;
    }

    //второй вариант
    public static int maxGapsCounter(String text) {
        return Arrays.stream(text.split("[^\\n&\\s]"))
                .map(String::length)
                .mapToInt(i -> i)
                .max()
                .orElse(0);
    }
}
