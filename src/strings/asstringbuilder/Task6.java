package strings.asstringbuilder;

public class Task6 {

    // Из заданной строки получить новую, повторив каждый символ дважды.
    public static String changeLineStringBuilder(String line) {
        StringBuilder builder = new StringBuilder();
        for (String s : line.split("")) {
            builder.append(s).append(s);
        }
        return builder.toString();
    }

}
