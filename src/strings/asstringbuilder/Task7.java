package strings.asstringbuilder;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Task7 {

    //Вводится строка. Требуется удалить из нее повторяющиеся символы и все пробелы.
    // Например, если было введено "abc cde def", то должно быть выведено "abcdef".

    //первый вариант
    public static String removeDuplicatesAndGaps(String line) {
        StringBuilder builder = new StringBuilder();
        for (String s : line.split("")) {
            if (!builder.toString().contains(s) && !s.equals(" ")) {
                builder.append(s);
            }
        }
        return builder.toString();
    }

    //второй вариант
    public static String removeGapsAndDuplicates(String line) {
        return Arrays.stream(line.replaceAll("\\s+", "").split(""))
                .distinct()
                .collect(Collectors.joining());
    }
}
