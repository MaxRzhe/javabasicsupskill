package strings.asstringbuilder;


import java.util.ArrayList;
import java.util.List;

public class Task8 {

    //Вводится строка слов, разделенных пробелами.
    //Найти самое длинное слово и вывести его на экран.
    //Случай, когда самых длинных слов может быть несколько, не обрабатывать.
    public static void findLongestWord(String text) {
        int maxLength = 0;
        List<String> words = new ArrayList<>();
        for (String s : text.split(" ")) {
            if (s.length() >= maxLength) {
                maxLength = s.length();
                words.add(s);
            }
        }
        if (words.size() != 1) {
            System.out.println(" ");
        } else {
            System.out.println(words.get(0));
        }
    }
}
