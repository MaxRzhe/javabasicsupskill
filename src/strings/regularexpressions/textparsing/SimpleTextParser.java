package strings.regularexpressions.textparsing;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SimpleTextParser {

    private static final String PARAGRAPH_PATTERN = "[^\n]+|[^\r\n]+[\n\r]+";
    private static final String SENTENCE_PATTERN = "[^\\.!?]+[.!?]+";
    private static final String WORDS_PATTERN = "[a-zA-Z1-9']+";
    private static final String SENTENCE_END_PATTERN = "[.!?]+";

    enum SortFlag {
        SORT_BY_LENGTH, SORT_BY_SYMBOL
    }

    enum CaseFlag {
        UPPER, LOWER, IGNORE
    }

    public static String sortParagraphs(String text) {
        List<String> paragraphs = findTextParts(text, PARAGRAPH_PATTERN, CaseFlag.IGNORE);
        return paragraphs.stream()
                .sorted(Comparator.comparingInt(paragraph -> findTextParts(paragraph, SENTENCE_PATTERN, CaseFlag.IGNORE).size()))
                .collect(Collectors.joining("\n\n"));
    }

    public static String sortSentencesByWords(String text, String symbol) {
        List<String> sentences = new ArrayList<>();
        Matcher matcher = Pattern.compile(SENTENCE_PATTERN).matcher(text);
        while (matcher.find()) {
            String sentence = matcher.group();
            if (symbol != null) {
                sentences.add(sortWords(sentence, SortFlag.SORT_BY_SYMBOL, symbol));
            } else {
                sentences.add(sortWords(sentence, SortFlag.SORT_BY_LENGTH, null));
            }
        }
        return String.join(" ", sentences);
    }

    private static List<String> findTextParts(String text, String pattern, CaseFlag flag) {
        List<String> parts = new ArrayList<>();
        Matcher matcher = Pattern.compile(pattern).matcher(text);
        while (matcher.find()) {
            String part = matcher.group();
            if (flag == CaseFlag.LOWER) {
                parts.add(part.toLowerCase());
            } else {
                parts.add(part);
            }
        }
        return parts;
    }

    private static String sortWords(String sentence, SortFlag flag, String symbol) {
        List<String> words = findTextParts(sentence, WORDS_PATTERN, CaseFlag.LOWER);
        List<String> ends = findTextParts(sentence, SENTENCE_END_PATTERN, CaseFlag.IGNORE);
        String end = !ends.isEmpty() && ends.get(0) != null ? ends.get(0) : "";
        switch (flag) {
            case SORT_BY_LENGTH:
                words.sort(String::compareTo);
                break;
            case SORT_BY_SYMBOL:
                words.sort((o1, o2) -> countSymbol(o2, symbol) - countSymbol(o1, symbol));
                break;
            default:
                break;
        }
        String sortedSentence = String.join(" ", words) + end;
        String firstLetter = String.valueOf(Character.toUpperCase(sortedSentence.charAt(0)));
        return firstLetter + sortedSentence.substring(1);
    }

    private static int countSymbol(String word, String symbol) {
        return word.length() - word.replaceAll(symbol, "").length();
    }
}
