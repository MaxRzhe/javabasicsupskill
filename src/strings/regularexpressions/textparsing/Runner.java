package strings.regularexpressions.textparsing;

import java.io.InputStream;

public class Runner {

    //Cоздать приложение, разбирающее текст (текст хранится в строке)
    // и позволяющее выполнять с текстом три различных операции:
    // отсортировать абзацы по количеству предложений;
    // в каждом предложении отсортировать слова по длине;
    // отсортировать лексемы в предложении по убыванию количества вхождений заданного символа,
    // а в случае равенства – по алфавиту.
    public static void main(String[] args) {
        InputStream stream = SimpleTextParser.class
                .getClassLoader()
                .getResourceAsStream("strings\\regularexpressions\\textparsing\\text");

        String text = TextReader.readText(stream);

        String sortedByParagraphs = SimpleTextParser.sortParagraphs(text);
        System.out.println(sortedByParagraphs);

        String sortedByWordsLength = SimpleTextParser.sortSentencesByWords(text, null);
        System.out.println(sortedByWordsLength);

        String sortedBySymbolFrequency = SimpleTextParser.sortSentencesByWords(text, "o");
        System.out.println(sortedBySymbolFrequency);

//
    }
}
