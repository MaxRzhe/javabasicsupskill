package strings.regularexpressions.xmlparsing;

import java.io.*;

public abstract class SimpleXmlParser {

    public static void parse(InputStream stream) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream))) {
            String line;
            while (reader.ready()) {
                line = reader.readLine().trim();
                if (isStartTag(line)) {
                    if (isTagWithAttribute(line)) {
                        parseTagWithAttribute(line);
                    } else {
                        parseStartTag(line);
                    }
                } else if (isFullTag(line)) {
                    parseFullTag(line);
                } else if (isSelfClosingTag(line)) {
                    parseSelfClosingTag(line);
                } else if (isFinishTag(line)) {
                    parseFinishTag(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean isStartTag(String line) {
        return line.startsWith("<") &&
                line.indexOf(">") == line.length() - 1 &&
                !line.contains("/");
    }

    private static boolean isFinishTag(String line) {
        return line.startsWith("</") &&
                line.endsWith(">");
    }

    private static boolean isSelfClosingTag(String line) {
        return line.startsWith("<") && line.endsWith("/>");
    }

    private static boolean isFullTag(String line) {
        return line.matches("<(.+)>[^<>/]+</\\1>");
    }

    private static boolean isTagWithAttribute(String line) {
        return (isStartTag(line) || isFullTag(line)) && line.matches("<[a-z-]+\\s+.+=.+>");
    }

    private static void parseFullTag(String line) {
        String tag = line.substring(1, line.indexOf(">")).trim();
        String value = line.substring(line.indexOf(">") + 1, line.lastIndexOf("<")).trim();
        System.out.printf("  Открывающий тег: %s\n", tag);
        System.out.printf("    Содержимое тега: %s\n", value);
        System.out.printf("  Закрывающий тег: %s\n", tag);
    }

    private static void parseStartTag(String line) {
        String tag = line.substring(1, line.length() - 1).trim();
        System.out.printf("Открывающий тег: %s\n", tag);
    }

    private static void parseFinishTag(String line) {
        String tag = line.substring(2, line.length() - 1).trim();
        System.out.printf("Закрывающий тег: %s\n", tag);
    }

    private static void parseSelfClosingTag(String line) {
        String tag = line.substring(1, line.indexOf("/")).trim();
        System.out.printf("  Тег без тела: %s\n", tag);
    }

    private static void parseTagWithAttribute(String line) {
        String tag = line.substring(1, line.indexOf(" ")).trim();
        String attributeName = line.substring(line.indexOf(" ") + 1, line.indexOf("=")).trim();
        String attributeValue = line.substring(line.indexOf("=") + 1, line.length() - 1).trim();
        System.out.printf("Открывающий тег: %s с аттрибутом. Имя аттрибута - %s, значение - %s.\n",
                tag, attributeName, attributeValue);
    }

}
