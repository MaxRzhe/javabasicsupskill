package strings.regularexpressions.xmlparsing;

import java.io.InputStream;

public class Runner {

    //Дана строка, содержащая следующий текст (xml-документ)
    //Напишите анализатор, позволяющий последовательно возвращать содержимое узлов xml-документа и его тип
    // (открывающий тег, закрывающий тег, содержимое тега, тег без тела).
    // Пользоваться готовыми парсерами XML для решения данной задачи нельзя
    public static void main(String[] args) {
        InputStream stream = SimpleXmlParser.class
                .getClassLoader()
                .getResourceAsStream("strings\\regularexpressions\\xmlparsing\\doc.xml");
        if (stream != null) {
            SimpleXmlParser.parse(stream);
        }
    }

}
