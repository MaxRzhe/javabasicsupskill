package algorithmization.arraysofarrays;

public class Task3 {

    //Дана матрица. Вывести k-ю строку и p-й столбец матрицы
    public static void print(int[][] matrix, int column, int row) {
        if (row < matrix.length && column < matrix[0].length) {
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[i].length; j++) {
                    if (i == row || j == column) {
                        System.out.print(matrix[i][j]);
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }
        }
    }
}
