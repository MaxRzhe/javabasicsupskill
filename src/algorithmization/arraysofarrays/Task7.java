package algorithmization.arraysofarrays;

public class Task7 {

    //Сформировать квадратную матрицу порядка n по правилу
    //A[i, j] = sin((i^2 - j^2) / n)
    //и подсчитать количество положительных элементов в ней.
    public static int formMatrix(int n) {
        int count = 0;
        double[][] matrix = new double[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = Math.sin(Math.toRadians((double) (i * i - j * j) / n));
                if (matrix[i][j] > 0) count ++;
            }
        }
        return count;
    }
}
