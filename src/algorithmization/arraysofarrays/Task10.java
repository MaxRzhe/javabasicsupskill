package algorithmization.arraysofarrays;

import java.util.ArrayList;
import java.util.List;

public class Task10 {

    //Найти положительные элементы главной диагонали квадратной матрицы.
    public static List<Integer> findPositiveElements(int[][] matrix) {
        List<Integer> elements = new ArrayList<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (i == j && matrix[i][j] > 0) {
                    elements.add(matrix[i][j]);
                }
            }
        }
        return elements;
    }
}
