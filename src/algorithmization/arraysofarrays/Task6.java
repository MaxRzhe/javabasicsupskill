package algorithmization.arraysofarrays;

public class Task6 {

    //Сформировать квадратную матрицу порядка n по заданному образцу(n - четное)
    public static int[][] formMatrix(int n) {
        int[][] matrix = new int[n][n];

        for (int i = 0; i < n / 2; i++) {
            for (int j = i; j < n - i; j++) {
                matrix[i][j] = 1;
            }
        }

        for (int i = n - 1; i >= n / 2; i--) {
            for (int j = n - i - 1; j < i + 1; j++) {
                matrix[i][j] = 1;
            }
        }
        return matrix;
    }
}
