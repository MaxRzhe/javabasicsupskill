package algorithmization.arraysofarrays;

import java.util.Scanner;

public class Task8 {

    private static final String FIRST_OFFER = "Please, enter the number of the first column(1-7):";
    private static final String SECOND_OFFER = "Please, enter the number of the second column(1-7):";

    //В числовой матрице поменять местами два столбца любых столбца,
    // т. е. все элементы одного столбца поставить на соответствующие им позиции другого,
    // а элементы второго переместить в первый. Номера столбцов вводит пользователь с клавиатуры.
    private static void changeColumns(int firstColumn, int secondColumn, int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            int tmp = matrix[i][firstColumn];
            matrix[i][firstColumn] = matrix[i][secondColumn];
            matrix[i][secondColumn] = tmp;
        }
    }

    private static void printMatrix(int[][] matrix) {
        for (int[] rows : matrix) {
            for (int inColumn : rows) {
                System.out.print(inColumn + " ");
            }
            System.out.println();
        }
    }

    private static class UserInputReader {
        private static final Scanner scanner;
        static {
            scanner = new Scanner(System.in);
        }

        private static int getUserInputNumber(int[][] matrix, int offerNum) {

            int matrixColumnsAmount = matrix[0].length;
            int num = 0;
            boolean isValidInput = false;

            String offer = offerNum == 1 ? FIRST_OFFER : SECOND_OFFER;
            System.out.println(offer);

            while (!isValidInput) {
                if (scanner.hasNextInt()) {
                    num = scanner.nextInt();
                    if (num < 1 || num > matrixColumnsAmount) {
                        System.out.println("The number must match the number of columns in the matrix");
                        System.out.println(offer);
                        continue;
                    }
                    isValidInput = true;
                } else {
                    System.out.println("Invalid input");
                    System.out.println(offer);
                }
            }
            return num - 1;
        }

        private static void close() {
            scanner.close();
        }

    }

    public static void main(String[] args) {
        int[][] matrix = new int[][]{
                {1, 0, 4, 7, 8, 7, 8},
                {2, 5, 4, 8, 2, 6, 7},
                {8, 7, 0, 5, 6, 9, 3},
                {7, 8, 5, 4, 1, 4, 6}
        };
        System.out.println("You can swap any two columns in the matrix:");

        printMatrix(matrix);

        int firstColumn = UserInputReader.getUserInputNumber(matrix, 1);
        int secondColumn = UserInputReader.getUserInputNumber(matrix, 2);

        UserInputReader.close();

        if (firstColumn != secondColumn) {
            changeColumns(firstColumn, secondColumn, matrix);
        }

        printMatrix(matrix);

    }
}
