package algorithmization.arraysofarrays;

public class Task15 {

    //Найдите наибольший элемент матрицы и заменить все нечетные элементы на него.
    public static void changeMatrix(int[][] matrix) {
        int maxElement = getMaxElement(matrix);

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] % 2 != 0) {
                    matrix[i][j] = maxElement;
                }
            }
        }
    }

    private static int getMaxElement(int[][] matrix) {
        int max = Integer.MIN_VALUE;
        for (int[] ints : matrix) {
            for (int anInt : ints) {
                max = Math.max(max, anInt);
            }
        }
        return max;
    }
}
