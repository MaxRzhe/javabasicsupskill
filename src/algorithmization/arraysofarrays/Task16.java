package algorithmization.arraysofarrays;

public class Task16 {

    //Магическим квадратом порядка n называется квадратная матрица размера nxn, составленная из чисел 1, 2, 3, ...,
    // так, что суммы по каждому столбцу, каждой строке и каждой из двух больших диагоналей равны между собой.
    // Построить такой квадрат.
    public static void formMagicSquare(int n) {
        if (n >= 3) {
            int[][] magicSquare = new int[n][n];
            if (n % 2 != 0) {
                magicSquare = createOddMagicSquare(n);
            } else if (n % 4 == 0) {
                magicSquare = createEvenMagicSquare(n);
            }
            print(magicSquare);
        }
    }


    private static int[][] createOddMagicSquare(int n) {
        int[][] magicSquare = new int[n][n];
        int i = n / 2;
        int j = n - 1;

        for (int num = 1; num <= n * n; ) {
            if (i == -1 && j == n) {
                j = n - 2;
                i = 0;
            } else {
                if (j == n) {
                    j = 0;
                }
                if (i < 0) {
                    i = n - 1;
                }
            }
            if (magicSquare[i][j] != 0) {
                j -= 2;
                i++;
            } else {
                magicSquare[i][j] = num++;
                j++;
                i--;
            }
        }
        return magicSquare;
    }

    private static int[][] createEvenMagicSquare(int n) {
        int[][] magicSquare = new int[n][n];
        int i, j;

        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                magicSquare[i][j] = (n * i) + j + 1;
            }
        }
        for (i = 0; i < n / 4; i++) {
            for (j = 0; j < n / 4; j++) {
                magicSquare[i][j] = (n * n + 1) - magicSquare[i][j];
            }
        }
        for (i = 0; i < n / 4; i++) {
            for (j = 3 * (n / 4); j < n; j++) {
                magicSquare[i][j] = (n * n + 1) - magicSquare[i][j];
            }
        }

        for (i = 3 * n / 4; i < n; i++) {
            for (j = 0; j < n / 4; j++) {
                magicSquare[i][j] = (n * n + 1) - magicSquare[i][j];
            }
        }

        for (i = 3 * n / 4; i < n; i++) {
            for (j = 3 * n / 4; j < n; j++) {
                magicSquare[i][j] = (n * n + 1) - magicSquare[i][j];
            }
        }

        for (i = n / 4; i < 3 * n / 4; i++) {
            for (j = n / 4; j < 3 * n / 4; j++) {
                magicSquare[i][j] = (n * n + 1) - magicSquare[i][j];
            }
        }
        return magicSquare;
    }

    private static void print(int[][] matrix) {
        for (int[] rows : matrix) {
            for (int inColumn : rows) {
                System.out.print(inColumn + " ");
            }
            System.out.println();
        }
    }

}
