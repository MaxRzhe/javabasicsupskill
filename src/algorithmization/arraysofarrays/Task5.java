package algorithmization.arraysofarrays;

public class Task5 {

    //Сформировать квадратную матрицу порядка n по заданному образцу
    public static int[][] formMatrix(int n) {
        int[][] matrix = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - i; j++) {
                matrix[i][j] = i + 1;
            }
        }
        return matrix;
    }
}
