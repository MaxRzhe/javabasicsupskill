package algorithmization.arraysofarrays;

import java.util.*;

public class Task13 {

    //Отсотрировать стобцы матрицы по возрастанию и убыванию значений эементов
    public static void sortColumns(int[][] matrix, boolean isAsc) {
        Map<Integer, List<Integer>> columnsMap = getColumnsMap(matrix);
        sortMapsValue(columnsMap, isAsc);
        for (int j = 0; j < matrix[0].length; j++) {
            for (int i = 0; i < matrix.length; i++) {
                matrix[i][j] = columnsMap.get(j).get(i);
            }
        }
    }

    private static Map<Integer, List<Integer>> getColumnsMap(int[][] matrix) {
        Map<Integer, List<Integer>> columnsMap = new HashMap<>();
        for (int j = 0; j < matrix[0].length; j++) {
            for (int i = 0; i < matrix.length; i++) {
                if (!columnsMap.containsKey(j)) {
                    columnsMap.put(j, new ArrayList<>(Collections.singletonList(matrix[i][j])));
                } else {
                    columnsMap.get(j).add(matrix[i][j]);
                }
            }
        }
        return columnsMap;
    }

    private static void sortMapsValue(Map<Integer, List<Integer>> map, boolean isAsc) {
        if(isAsc) {
            map.forEach((key, value) -> value.sort((o1, o2) -> o1 - o2));
        } else {
            map.forEach((key, value) -> value.sort((o1, o2) -> o2 - o1));
        }
    }
}
