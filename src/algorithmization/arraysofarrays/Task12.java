package algorithmization.arraysofarrays;

public class Task12 {

    //Отсортировать строки матрицы по возрастанию и убыванию значений элементов
    public static void sortRows(int[][] matrix, boolean isAsc) {
        for (int[] row : matrix) {
            sortElements(row, isAsc);
        }
    }

    private static void sortElements(int[] array, boolean isAsc) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                boolean condition = isAsc ? array[j] > array[j + 1] : array[j] < array[j + 1];
                if (condition) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
    }
}
