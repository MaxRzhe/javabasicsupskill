package algorithmization.arraysofarrays;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Task14 {

    //Сформировать случайную матрицу m x n, состоящую из нулей и единиц,
    // причем в каждом столбце число единиц равно номеру столбца
    public static int[][] formMatrix() {
            int m = new Random().nextInt(20);
            int n = new Random().nextInt(20);

        int[][] matrix = new int[m][n];

        for (int j = 0; j < matrix[0].length; j++) {
            Set<Integer> indexes = generateIndexes(j, matrix.length);
            for (Integer i : indexes) {
                matrix[i][j] = 1;
            }
        }
        return matrix;
    }

    private static Set<Integer> generateIndexes(int n, int limit) {
        Set<Integer> indexes = new HashSet<>();
        while (indexes.size() < n) {
            indexes.add(new Random().nextInt(limit));
        }
        return indexes;
    }
}
