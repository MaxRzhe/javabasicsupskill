package algorithmization.arraysofarrays;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Task11 {

    //Матрицу 10x20 заполнить случайными числами от 0 до 15.
    // Вывести на экран саму матрицу и номера строк, в которых число 5 встречается три и более раз
    public static void solveMatrix() {
        int[][] matrix = new int[10][20];
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = new Random().nextInt(16);
                if (matrix[i][j] == 5) {
                    map.merge(i, 1, Integer::sum);
                }
            }
        }

        print(matrix);
        map.entrySet()
                .stream()
                .filter(entry -> entry.getValue() >= 3)
                .forEach(entry -> System.out.println(entry.getKey()));
    }

    private static void print(int[][] matrix) {
        for (int[] ints : matrix) {
            for (int anInt : ints) {
                System.out.print(anInt + " ");
            }
            System.out.println();
        }
    }
}
