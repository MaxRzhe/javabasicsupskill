package algorithmization.onedimensionalarrays;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Task9 {

    //В массиве целых чисел с количеством элементов n найти наиболее часто встречающееся число. Если таких
    //чисел несколько, то определить наименьшее из них
    public static int findNumber(int[] array) {
        Map<Integer, Long> frequencyMap = createFrequencyMap(array);

        long maxFrequency = Collections.max(frequencyMap.values());

        return frequencyMap.entrySet().stream()
                .filter(entry -> entry.getValue() == maxFrequency)
                .sorted(Comparator.comparingInt(Map.Entry::getKey))
                .limit(1)
                .map(Map.Entry::getKey)
                .findFirst()
                .orElse(Integer.MIN_VALUE);
    }

    private static Map<Integer, Long> createFrequencyMap(int[] array) {
        Map<Integer, Long> frequencyMap = new HashMap<>();
        if (array.length != 0) {
            frequencyMap = Arrays.stream(array)
                    .boxed()
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        }
        return frequencyMap;
    }
}
