package algorithmization.onedimensionalarrays;

import java.util.stream.IntStream;

public class Task6 {


    //Задана последовательность N вещественных чисел. Вычислить сумму чисел, порядковые номера которых
    //являются простыми числами
    public static double findSumPrimesIndexes(double[] array) {
        return array.length < 3
                ? 0
                : IntStream.range(0, array.length)
                .filter(Task6::isPrime)
                .mapToDouble(x -> array[x])
                .sum();
    }

    private static boolean isPrime(int x) {
        if (x < 2) return false;
        if (x == 2) return true;
        for (int i = 2; i < x; i++) {
            if (x % i == 0) {
                return false;
            }
        }
        return true;
    }
}
