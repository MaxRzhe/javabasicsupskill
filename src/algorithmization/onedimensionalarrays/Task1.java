package algorithmization.onedimensionalarrays;

import java.util.Arrays;

public class Task1 {

    //В массив A [N] занесены натуральные числа. Найти сумму тех элементов, которые кратны данному К
    public static int getSumNumbers(int[] a, int k) {
        return Arrays.stream(a).filter(x -> x % k == 0).sum();
    }
}
