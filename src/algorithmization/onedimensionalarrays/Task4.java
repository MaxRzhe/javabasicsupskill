package algorithmization.onedimensionalarrays;

import java.util.Arrays;

public class Task4 {

    //Даны действительные числа а1 ,а2 ,..., аn . Поменять местами наибольший и наименьший элементы
    public static int[] changeMinMax(int[] array) {
        int min = Arrays.stream(array).min().orElse(Integer.MIN_VALUE);
        int max = Arrays.stream(array).max().orElse(Integer.MAX_VALUE);
        for (int i = 0; i < array.length; i++) {
            if (array[i] == min) {
                array[i] = max;
            } else if (array[i] == max) {
                array[i] = min;
            }
        }
        return array;
    }
}
