package algorithmization.onedimensionalarrays;

public class Task7 {


    //Даны действительные числа a1,a2,...,an . Найти max( a1 + a2n,a2 + a2n−1,...,an + an+1)
    public static double findMax(double[] arr) {
        double max = Double.MIN_VALUE;
        if (arr.length == 1) {
            return arr[0];
        }
        if (arr.length == 2) {
            return arr[0] + arr[1];
        }
        int count = arr.length % 2 == 0 ? arr.length / 2 : arr.length / 2 + 1;

        for (int i = 0, j = arr.length - 1; i < count; i++, j--) {
            double num = i != j ? arr[i] + arr[j] : arr[i];
            if (num > max) {
                max = num;
            }
        }
        return max;
    }
}

