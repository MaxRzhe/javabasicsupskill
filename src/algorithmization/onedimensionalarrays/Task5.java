package algorithmization.onedimensionalarrays;

public class Task5 {

    //Даны целые числа а1 ,а2 ,..., аn . Вывести на печать только те числа, для которых аi > i
    public static void printNums(int[] array) {
        for (int i = 1; i <= array.length; i++) {
            if (array[i - 1] > i) {
                System.out.println(array[i - 1]);
            }
        }
    }
}
