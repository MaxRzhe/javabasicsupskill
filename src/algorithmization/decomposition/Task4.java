package algorithmization.decomposition;

public class Task4 {

    //На плоскости заданы своими координатами n точек.
    // Написать метод(методы), определяющие, между какими из пар точек самое большое расстояние.
    // Указание. Координаты точек занести в массив.
    public static int[][] findMaxDotsDistance(int[][] dotsArray) {
        int[][] result = new int[2][2];
        double maxDistance = Double.MIN_VALUE;
        for (int i = 0; i < dotsArray.length; i++) {
            for (int j = i; j < dotsArray.length; j++) {
                double distance = findDistance(dotsArray[i], dotsArray[j]);
                if (distance > maxDistance) {
                    maxDistance = distance;
                    result[0] = dotsArray[i];
                    result[1] = dotsArray[j];
                }
            }
        }
        return result;
    }

    private static double findDistance(int[] pointA, int[] pointB) {
        return Math.sqrt(Math.pow((pointA[0] - pointB[0]), 2) + Math.pow((pointA[1] - pointB[1]), 2));
    }
}
