package algorithmization.decomposition;

public class Task2 {

    //Написать метод(методы) для нахождения наибольшего общего делителя четырех натуральных чисел.
    public static int findGCD(int a, int b, int c, int d) {
        return findGCD(a, findGCD(b, findGCD(c, d)));
    }

    public static int findGCD2(int a, int b, int c, int d) {
        int[] arr = new int[]{a, b, c, d};
        int result = 0;
        for (int i : arr) {
            result = findGCD(result, i);
            if (result == 1) {
                return 1;
            }
        }
        return result;
    }

    public static int findGCD(int a, int b) {
        if (b == 0) {
            return a;
        }
        return findGCD(b, a % b);
    }
}
