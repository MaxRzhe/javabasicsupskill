package algorithmization.decomposition;

public class Task16 {

    // Написать программу, определяющую сумму n - значных чисел, содержащих только нечетные цифры.
    // определить также, сколько четных цифр в найденной сумме.
    // Для решения задачи использовать декомпозицию
    public static void findSum(int digits) {
        int sum = 0;
        int start = (int) Math.pow(10, digits - 1);
        int finish = (int) Math.pow(10, digits);
        for (int i = start; i < finish; i++) {
            if (isOddDigits(i)) {
                sum += i;
            }
        }
        System.out.printf("The sum: %d.\n", sum);
        int evenCount = countEvenDigits(sum);
        System.out.printf("There %s %d even number%s in the sum.\n",
                evenCount > 1 ? "are" : "is",
                evenCount,
                evenCount > 1 ? "s" : "");
    }

    private static boolean isOddDigits(int n) {
        while (n > 0) {
            if (n % 10 % 2 == 0) {
                return false;
            } else {
                n /= 10;
            }
        }
        return true;
    }

    private static int countEvenDigits(int n) {
        int count = 0;
        while (n > 0) {
            if (n % 10 % 2 == 0) {
                count++;
            }
            n /= 10;
        }
        return count;
    }
}
