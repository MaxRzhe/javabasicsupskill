package algorithmization.decomposition;

import java.util.ArrayList;
import java.util.List;

public class Task14 {

    //Натуральное число, в записи которого n цифр, называется числом Армстронга, если сумма его цифр,
    //возведенная в степень n, равна самому числу. Найти все числа Армстронга от 1 до k.
    //Для решения задачи использовать декомпозицию.
    public static List<Integer> findArmstrongNUms(int k) {
        List<Integer> result = new ArrayList<>();
        for (int i = 1; i <= k; i++) {
            if (findProductsSum(i) == i) {
                result.add(i);
            }
        }
        return result;
    }

    private static int findProductsSum(int n) {
        int power = countDigits(n);
        int sum = 0;
        while (n > 0) {
            sum += Math.pow(n % 10, power);
            n /= 10;
        }
        return sum;
    }

    private static int countDigits(int n) {
        int count = 0;
        while (n > 0) {
            n /= 10;
            count++;
        }
        return count;
    }
}
