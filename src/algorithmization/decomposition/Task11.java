package algorithmization.decomposition;

public class Task11 {

    //Написать метод(методы), определяющий, в каком из данных двух чисел больше цифр.
    public static int compareLength(int a, int b) {
        return Integer.compare(length(a), length(b));
    }

    private static int length(int n) {
        return String.valueOf(n).length();
    }
}
