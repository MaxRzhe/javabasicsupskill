package algorithmization.decomposition;

import java.util.ArrayList;
import java.util.List;

public class Task12 {

    //Даны натуральные числа К и N.
    //Написать метод(методы) формирования массива А, элементами которого являются числа,
    //сумма цифр которых равна К и которые не большее N.
    public static List<Integer> create(int k, int n) {
        List<Integer> result = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            if (findDigitsSum(i)  == k) {
                result.add(i);
            }
        }
        return result;
    }

    private static int findDigitsSum(int n) {
        int sum = 0;
        while (n > 0) {
            sum += n % 10;
            n /= 10;
        }
        return sum;
    }
}
