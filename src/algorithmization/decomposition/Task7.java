package algorithmization.decomposition;

public class Task7 {

    //Написать метод(методы) для вычисления суммы факториалов всех нечетных чисел от 1 до 9
    public static int findSumFactorials() {
        int sum = 0;
        for (int i = 1; i <= 9; i++) {
            if (i % 2 != 0) {
                sum += findFactorial(i);
            }
        }
        return sum;
    }

    private static int findFactorial(int n) {
        if (n == 1) {
            return n;
        }
        return n * findFactorial(n - 1);
    }
}
