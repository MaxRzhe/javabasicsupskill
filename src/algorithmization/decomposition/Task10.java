package algorithmization.decomposition;

import java.util.Arrays;

public class Task10 {

    //Дано натуральное число N. Написать метод(методы) для формирования массива,
    // элементами которого являются цифры числа N.
    public static int[] decomposeNumber(int n) {
        int[] result = new int[String.valueOf(n).length()];
        int index = result.length - 1;
        while (n > 0) {
            result[index] = n % 10;
            n /= 10;
            index--;
        }
        return result;
    }
}
