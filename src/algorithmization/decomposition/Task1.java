package algorithmization.decomposition;

import java.util.ArrayList;
import java.util.List;

public class Task1 {

    //Написать метод(методы) для нахождения наибольшего общего делителя и наименьшего общего кратного двух натуральных чисел;
    public static int findGCD(int a, int b) {
        if (b == 0) {
            return a;
        }
        return findGCD(b, a % b);
    }

    public static int findLCM(int a, int b) {
        return (a * b) / findGCD(a, b);
    }
}
