package algorithmization.decomposition;

public class Task17 {

    //Из заданного числа вычли сумму его цифр. Из результата вновь вычли сумму его цифр и т.д.
    // Сколько таких действий надо произвести, чтобы получился нуль?
    // Для решения задачи использовать декомпозицию

    public static int countSteps(int n) {
        int count = 0;
        while (n > 0) {
            n = n - findDigitsSum(n);
            count++;
        }
        return count;
    }

    private static int findDigitsSum(int n) {
        int sum = 0;
        while (n > 0) {
            sum += n % 10;
            n /= 10;
        }
        return sum;
    }
}
