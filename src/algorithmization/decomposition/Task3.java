package algorithmization.decomposition;

public class Task3 {

    //Вычислить площадь правильного шестиугольника со стороной а, используя метод вычисления площади треугольника.
    public static double findHexagonSquare(int side) {
        return findTriangleSquare(side) * 6;
    }

    public static double findTriangleSquare(int side) {
        return side * side * Math.sqrt(3) / 4;
    }
}
