package algorithmization.decomposition;

public class Task6 {

    //Написать метод(методы), проверяющий, являются ли данные три числа взаимно простыми.
    public static boolean isCoPrime(int a, int b, int c) {
        return findGCD(a, findGCD(b, c)) == 1;
    }

    public static int findGCD(int a, int b) {
        if (b == 0) {
            return a;
        }
        return findGCD(b, a % b);
    }
}
