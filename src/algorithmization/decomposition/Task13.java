package algorithmization.decomposition;

public class Task13 {

    //Два простых числа называются «близнецами», если они отличаются друг от друга на 2 (например, 41 и 43).
    // Найти и напечатать все пары «близнецов» из отрезка [n,2n], где n - заданное натуральное число больше 2.
    // Для решения задачи использовать декомпозицию.
    public static void solveTwins(int n) {
        for (int i = n; i <= 2 * n; i++) {
            print(i, i + 2);
        }
    }

    private static void print(int a, int b) {
        System.out.printf("Twins: %d - %d\n", a, b);
    }
}
