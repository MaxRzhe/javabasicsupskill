package algorithmization.decomposition;

public class Task8 {

    //Задан массив D. Определить следующие суммы: D[l] + D[2] + D[3]; D[3] + D[4] + D[5]; D[4] +D[5] +D[6].
    public static void calculateSums(int[] arr) {
        int firstSum = findSum(arr, 1, 3);
        int secondSum = findSum(arr, 3, 5);
        int thirdSum = findSum(arr, 4, 6);
        System.out.printf("First sum: %d\n", firstSum);
        System.out.printf("Second sum: %d\n", secondSum);
        System.out.printf("Third sum: %d\n", thirdSum);
    }

    private static int findSum(int[] arr, int start, int finish) {
        int sum = Integer.MIN_VALUE;
        if ((start >= 0 && start < arr.length - 1) && (finish > 0 && finish <= arr.length - 1) && start <= finish) {
            for (int i = start; i <= finish; i++) {
                sum += arr[start];
            }
        }
        return sum;
    }
}
