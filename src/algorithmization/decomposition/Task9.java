package algorithmization.decomposition;

public class Task9 {

    //Даны числа X, Y, Z, Т — длины сторон четырехугольника.
    // Написать метод(методы) вычисления его площади, если угол между сторонами длиной X и Y— прямой
    public static double findRectangleSquare(double x, double y, double z, double t) {
        double diagonal = Math.sqrt(x * x + y * y);
        double s1 = findTriangleSquare(diagonal, z, t);
        double s2 = 0.5 * x * y;
        return s1 + s2;
    }

    private static double findTriangleSquare(double a, double b, double c) {
        double p = findHalfPerimeter(a, b, c);
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    private static double findHalfPerimeter(double a, double b, double c) {
        return (a + b + c) / 2;
    }


}
