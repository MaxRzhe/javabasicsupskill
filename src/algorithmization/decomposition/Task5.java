package algorithmization.decomposition;


import java.util.Arrays;

public class Task5 {

    //Составить программу, которая в массиве A[N] находит второе по величине число
    // (вывести на печать число, которое меньше максимального элемента массива, но больше всех других элементов)
    public static void findSecondMax(int[] arr) {
        if (arr.length >= 3) {
            Arrays.sort(arr);
            int result = arr[arr.length - 1];
            for (int i = arr.length - 2; i >= 0; i--) {
                if (result != arr[i]) {
                    result = arr[i];
                    break;
                }
            }
            if (result == arr[0]) {
                System.out.println("No such element");
            } else {
                System.out.printf("Searched element: %d", result);
            }
        } else {
            System.out.println("Improper length of the array");
        }
    }
}
