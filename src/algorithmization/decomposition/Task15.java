package algorithmization.decomposition;

import java.util.ArrayList;
import java.util.List;

public class Task15 {

    //Найти все натуральные n-значные числа, цифры в которых образуют
    //строго возрастающую последовательность (например, 1234, 5789).
    // Для решения задачи использовать декомпозицию.

    public static List<Integer> findNums(int digits) {
        List<Integer> resultList = new ArrayList<>();
        int start = (int) Math.pow(10, digits - 1);
        int finish = (int) Math.pow(10, digits);
        for (int i = start; i < finish; i++) {
            if (isSearchedNum(i)) {
                resultList.add(i);
            }
        }
        return resultList;
    }

    private static boolean isSearchedNum(int n) {
        int previous = n % 10 + 1;
        while (n > 0) {
            int next = n % 10;
            if (previous <= next) {
                return false;
            }
            previous = next;
            n /= 10;
        }
        return true;
    }
}
