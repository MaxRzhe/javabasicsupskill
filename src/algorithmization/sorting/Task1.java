package algorithmization.sorting;

import java.util.List;

public class Task1 {

    //Заданы два одномерных массива с различным количеством элементов и натуральное число k.
    // Объединить их в один массив, включив второй массив между k-м и (k+1) - м элементами первого,
    // при этом не используя дополнительный массив.
    public static List<Integer> mergeArrays(List<Integer> list1, List<Integer> list2, int k) {
        if (k == list1.size() - 1) {
            list1.addAll(list2);
        }
        if (k >= 0 && k < list1.size() - 2) {
            int index = list1.size() - 1;
            while (index != k) {
                list2.add(list1.get(index));
                list1.remove(index);
                index--;
            }
            list1.addAll(list2);
        }
        return list1;
    }
}
