package algorithmization.sorting;

import java.util.Arrays;

public class Task5 {

    //сортировка Шелла
    public static void shellSort(int[] arr) {
        int gap = arr.length / 2;

        while (gap >= 1) {
            for (int i = 0; i < arr.length; i++) {
                for (int j = i - gap; j >= 0; j--) {
                    if (arr[j] > arr[j + gap]) {
                        int tmp = arr[j];
                        arr[j] = arr[j + gap];
                        arr[j + gap] = tmp;
                    }
                }
            }
            gap = gap / 2;
        }
    }
}
