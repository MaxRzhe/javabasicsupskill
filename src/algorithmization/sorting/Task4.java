package algorithmization.sorting;

import java.util.Arrays;

public class Task4 {

    //Реализуйте сортировку вставками
    public static void insertionSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int j = i;
            int val = arr[i];
            while (j > 0 && arr[j - 1] >= val) {
                arr[j] = arr[j - 1];
                j--;
            }
            arr[j] = val;
        }
    }
}
