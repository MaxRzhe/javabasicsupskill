package basicoop.paymentapp;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Payment {
    private static final Set<Integer> idCache = new HashSet<>();

    private final Map<Integer, PaymentItem> paymentMap;
    private final int paymentNumber;

    private String recipient;
    private String sender;
    private LocalDateTime paymentDate;

    public Payment(String recipient, String sender, LocalDateTime paymentDate) {
        this.paymentNumber = generateId();
        this.recipient = recipient;
        this.sender = sender;
        this.paymentDate = paymentDate;
        this.paymentMap = new HashMap<>();
    }

    public int getPaymentNumber() {
        return paymentNumber;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    private static class PaymentItem {
        private final String name;
        private final String description;
        private final double price;
        private int amount;


        public PaymentItem(String name, String description, double price) {
            this.name = name;
            this.description = description;
            this.price = price;
            this.amount = 1;
        }

        @Override
        public String toString() {
            return String.format("------\nName: %s\nDescription: %s\nPrice: $ %.2f\nAmount: %d pсs.\n------\n",
                    name, description, price, amount);
        }

    }

    public void addItem(Item item) {
        if (item != null) {
            if (!paymentMap.containsKey(item.getId())) {
                paymentMap.put(item.getId(), new PaymentItem(item.getName(), item.getDescription(), item.getPrice()));
            } else {
                paymentMap.get(item.getId()).amount += 1;
            }
        }
    }

    public void removeItem(Item item) {
        if (item != null && paymentMap.containsKey(item.getId())) {
            if (paymentMap.get(item.getId()).amount == 1) {
                paymentMap.remove(item.getId());
            } else {
                paymentMap.get(item.getId()).amount -= 1;
            }
        }
    }

    public double totalPrice() {
        return paymentMap.values().stream()
                .mapToDouble(item -> item.price)
                .sum();
    }

    public void paymentInfo() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        String date = paymentDate.format(formatter);
        System.out.printf("******\nPayment #%d dated %s.\nRecipient: %s.\nSender: %s.\n\n",
                getPaymentNumber(), date, getRecipient(), getSender());
        System.out.println("Items:\n");
        paymentMap.values().forEach(System.out::println);
        System.out.printf("Total price: $ %.2f\n", totalPrice());
        System.out.println("*****\n");
    }

    private int generateId() {
        Random random = new Random();
        int id;
        int cacheSize = idCache.size();
        do {
            id = random.nextInt();
            if (id > 0) {
                idCache.add(id);
            }
        } while (idCache.size() != cacheSize + 1);
        return id;
    }
}
