package basicoop.paymentapp;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

//Создать класс Payment с внутренним классом, с помощью объектов которого
//можно сформировать покупку из нескольких товаров.
public class PaymentApp {

    private static final List<Item> itemList = new ArrayList<>();

    static {
        Item item1 = new Item(1, "Sunglasses", "The great item for vacation!", 19.99);
        itemList.add(item1);
        itemList.add(item1);
        Item item2 = new Item(2, "Belt", "The best leather belt!", 49.99);
        itemList.add(item2);
        Item item3 = new Item(3, "Book", "An interesting book for rainy evening!", 9.99);
        itemList.add(item3);
        Item item4 = new Item(4, "Spoon", "A tea spoon which made of silver!", 99.99);
        itemList.add(item4);
        Item item5 = new Item(5, "Cup", "A big big big cup!", 12.99);
        itemList.add(item5);
        itemList.add(item5);
        itemList.add(item5);
        Item item6 = new Item(6, "Mouse", "A computer funny mouse!", 15.99);
        itemList.add(item6);
    }

    public static void main(String[] args) {
        Payment payment = new Payment("Philip Vorobyov", "Maksim Rzhevutski", LocalDateTime.now());
        for (Item item : itemList) {
            payment.addItem(item);
        }

        payment.paymentInfo();

        payment.removeItem(itemList.get(0));
        payment.removeItem(itemList.get(4));
        payment.removeItem(itemList.get(6));

        payment.paymentInfo();

    }
}
