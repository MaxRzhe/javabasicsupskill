package basicoop.dragonapp;

public class Treasure {
    private final String name;
    private final int value;

    public Treasure(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.format("Name: %s, Value: %d", name, value);
    }
}
