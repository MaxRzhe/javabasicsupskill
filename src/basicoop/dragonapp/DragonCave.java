package basicoop.dragonapp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DragonCave {
    private List<Treasure> treasures;
    private int totalValue;

    private DragonCave() {
        this.treasures = new ArrayList<>();
        this.totalValue = 0;
    }

    public int getTotalValue() {
        return totalValue;
    }

    public static DragonCave newInstance() {
        return new DragonCave();
    }

    public void setTreasures(List<Treasure> treasures) {
        this.treasures = treasures;
        this.totalValue = totalValue();
    }


    public List<Treasure> mostExpensive() {
        if (treasures != null && !treasures.isEmpty()) {
            sortTreasures(true);
            return treasures.stream()
                    .filter(treasure -> treasure.getValue() == treasures.get(0).getValue())
                    .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }



    public void treasuresInfo() {
        treasures.forEach(System.out::println);
    }

    public List<Treasure> makeSelection(int sum) {
        if (sum < totalValue) {
            sortTreasures(false);

            int amount = 0;
            List<Treasure> result = new ArrayList<>();

            for (Treasure treasure : treasures) {
                int value = treasure.getValue();
                if (amount + value < sum) {
                    amount += value;
                    result.add(treasure);
                } else {
                    return result;
                }
            }
            return result;
        } else if (sum == totalValue) {
            return treasures;
        } else {
            return Collections.emptyList();
        }
    }

    private int totalValue() {
        return treasures.stream().mapToInt(Treasure::getValue).sum();
    }


    private void sortTreasures(boolean desc) {
        if (!desc) {
            treasures.sort(Comparator.comparingInt(Treasure::getValue));
        } else {
            treasures.sort((t1, t2) -> t2.getValue() - t1.getValue());
        }
    }


}
