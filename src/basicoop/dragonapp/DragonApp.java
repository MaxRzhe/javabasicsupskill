package basicoop.dragonapp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

//Создать программу, позволяющую обрабатывать сведения о 100 сокровищах в пещере дракона.
//Реализовать возможность просмотра сокровищ, выбора самого дорогого по стоимости сокровища
//и выбора сокровищ на заданную сумму.
public class DragonApp {
    private static final List<Treasure> treasures = new ArrayList<>();

    static {
        Random random = new Random();
        for (int i = 1; i <= 100 ; i++) {
            treasures.add(new Treasure(String.format("treasure #%d", i), random.nextInt(100) + 1));
        }
        Collections.shuffle(treasures);
    }


    public static void main(String[] args) {
        DragonCave cave = DragonCave.newInstance();
        cave.setTreasures(treasures);

        cave.treasuresInfo();
        System.out.println(cave.getTotalValue());

        List<Treasure> mostExpensive = cave.mostExpensive();
        mostExpensive.forEach(System.out::println);

        List<Treasure> byTotalSum = cave.makeSelection(2000);
        byTotalSum.forEach(System.out::println);
        System.out.println(byTotalSum.stream().mapToInt(Treasure::getValue).sum());

    }
}
