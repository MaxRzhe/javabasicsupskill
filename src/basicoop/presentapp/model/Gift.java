package basicoop.presentapp.model;

import java.util.List;

public class Gift {
    private final String name;
    private final Pack pack;
    private final List<Sweet> sweets;

    public Gift(String name, List<Sweet> sweets, Pack pack) {
        this.name = name;
        this.sweets = sweets;
        this.pack = pack;
    }

    public String getName() {
        return name;
    }

    public List<Sweet> getSweets() {
        return sweets;
    }

    public Pack getPack() {
        return pack;
    }

    public double totalPrice() {
        return sweets.stream().mapToDouble(Sweet::getPrice).sum() + pack.getPrice();
    }

    public double totalWeight() {
        return sweets.stream().mapToDouble(Sweet::getAmountGrams).sum();
    }

    public void giftInfo() {
        System.out.println("\n*****The whole order*****");
        System.out.printf("Gift with label: %s\n", getName());
        System.out.println("Sweets:");
        getSweets().forEach(System.out::println);
        System.out.println("Pack: " + getPack());
        System.out.printf("Total price: $ %.2f\n", totalPrice());
        System.out.printf("Total weight: %.2fg\n", totalWeight());
        System.out.println("*****Thank you for order*****\n");
    }
}
