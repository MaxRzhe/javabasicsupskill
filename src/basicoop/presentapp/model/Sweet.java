package basicoop.presentapp.model;

import basicoop.presentapp.enums.SweetType;

public class Sweet {
    private String name;
    private double amountGrams;
    private double price;
    private final SweetType sweetType;

    public Sweet(SweetType sweetType, double amountGrams) {
        this.name = sweetType.name();
        this.amountGrams = amountGrams;
        this.sweetType = sweetType;
        recountPrice(amountGrams);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmountGrams() {
        return amountGrams;
    }

    public void setAmountGrams(double amountGrams) {
        this.amountGrams = amountGrams;
        recountPrice(amountGrams);
    }

    public double getPrice() {
        return price;
    }

    private void recountPrice(double amountGrams) {
        price = sweetType.getPricePerKg() * amountGrams / 1000;
    }

    @Override
    public String toString() {
        return String.format("%s, %.2fg, $ %.2f", name, amountGrams, price);
    }
}
