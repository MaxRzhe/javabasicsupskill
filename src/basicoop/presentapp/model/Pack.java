package basicoop.presentapp.model;

import basicoop.presentapp.enums.PackColor;
import basicoop.presentapp.enums.PackType;

public class Pack {
    private String name;
    private double capacityGrams;
    private double price;
    private PackType packType;
    private PackColor color;

    public Pack(PackType packType, PackColor color) {
        this.color = color;
        this.packType = packType;
        updateFields(packType);
    }

    public PackColor getColor() {
        return color;
    }

    public void setColor(PackColor color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public double getCapacityGrams() {
        return capacityGrams;
    }

    public double getPrice() {
        return price;
    }

    public void changePackType(PackType packType) {
        this.packType = packType;
        updateFields(packType);
    }

    private void updateFields(PackType type) {
        name = type.name();
        capacityGrams = type.getCapacityGrams();
        price = type.getPrice();
    }

    @Override
    public String toString() {
        return String.format("%s, %s, $ %.2f", name, color, price);
    }
}

