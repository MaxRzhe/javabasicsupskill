package basicoop.presentapp.output;

import basicoop.presentapp.enums.PackColor;
import basicoop.presentapp.enums.PackType;
import basicoop.presentapp.model.Gift;
import basicoop.presentapp.model.Sweet;
import basicoop.presentapp.output.factory.UserOutputFactory;

import java.util.List;
import java.util.Map;

public class UserOutputHandler {
    private final UserOutputWriter writer;

    public UserOutputHandler(UserOutputFactory factory) {
        this.writer = factory.createOutputWriter();
    }

    public void showSweetPreselection(String sweetType, double amount){
        writer.showSweetPreselection(sweetType, amount);
    }

    public void showAvailableSweets() {
        writer.showAvailableSweets();
    }

    public void showOrderedSweets(Map<String, Sweet> sweetMap) {
        writer.showOrderedSweets(sweetMap);
    }

    public void showAvailablePacks(List<PackType> availablePacks) {
        writer.showAvailablePacks(availablePacks);
    }

    public void showOrderedPack(PackType packType) {
        writer.showOrderedPack(packType);
    }

    public void showAvailableColors() {
        writer.showAvailableColors();
    }

    public void showOrderColor(PackColor color) {
        writer.showOrderColor(color);
    }

    public void printWholeOrderInfo(Gift gift) {
        writer.printWholeOrderInfo(gift);
    }
}
