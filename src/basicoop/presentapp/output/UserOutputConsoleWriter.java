package basicoop.presentapp.output;

import basicoop.presentapp.enums.PackColor;
import basicoop.presentapp.enums.PackType;
import basicoop.presentapp.enums.SweetType;
import basicoop.presentapp.model.Gift;
import basicoop.presentapp.model.Sweet;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class UserOutputConsoleWriter implements UserOutputWriter{
    @Override
    public void showSweetPreselection(String sweetType, double amount) {
        System.out.printf("You chose: %s, %.2fg\n", sweetType, amount);
    }

    @Override
    public void showAvailableSweets() {
        Arrays.stream(SweetType.values())
                .forEach(type -> System.out.printf("\t%s - for %s, price for 1kg: $ %.2f\n",
                        type.shortCut(), type.name(), type.getPricePerKg()));
    }

    @Override
    public void showOrderedSweets(Map<String, Sweet> sweetMap) {
        System.out.println("*****\nYou have chosen: ");
        sweetMap.forEach((key, value) -> System.out.printf("\t%s, %.2fg, $ %.2f\n",
                key, value.getAmountGrams(), value.getPrice()));

        double wholePrice = sweetMap.values().stream().mapToDouble(Sweet::getPrice).sum();
        System.out.printf("Whole price without pack: $ %.2f\n", wholePrice);

        double wholeWeight = sweetMap.values().stream().mapToDouble(Sweet::getAmountGrams).sum();
        System.out.printf("Whole weight: %.2fg\n*****\n\n", wholeWeight);
    }

    @Override
    public void showAvailablePacks(List<PackType> availablePack) {
        availablePack.forEach(type -> System.out.printf("\t%d - for %s, capacity: %.2fg, price: $ %.2f\n",
                type.ordinal(), type.name(), type.getCapacityGrams(), type.getPrice()));
    }

    @Override
    public void showOrderedPack(PackType packType) {
        System.out.printf("\n*****\nYou chose pack:\n\tsize: %s\n\tcapacity: %.2fg\n\tprice: $ %.2f\n*****\n\n",
                packType.name(), packType.getCapacityGrams(), packType.getPrice());
    }

    @Override
    public void showAvailableColors() {
        Arrays.stream(PackColor.values()).forEach(packColor -> System.out.printf("\t%d - for %s\n",
                packColor.ordinal(), packColor.name()));
    }

    @Override
    public void showOrderColor(PackColor color) {
        System.out.printf("You chose the pack color: %s\n", color.name());
    }

    @Override
    public void printWholeOrderInfo(Gift gift) {
        gift.giftInfo();
    }
}
