package basicoop.presentapp.output;

import basicoop.presentapp.enums.PackColor;
import basicoop.presentapp.enums.PackType;
import basicoop.presentapp.model.Gift;
import basicoop.presentapp.model.Sweet;

import java.util.List;
import java.util.Map;

public interface UserOutputWriter {
    void showSweetPreselection(String sweetType, double amount);

    void showAvailableSweets();

    void showOrderedSweets(Map<String, Sweet> sweetMap);

    void showAvailablePacks(List<PackType> availablePack);

    void showOrderedPack(PackType packType);

    void showAvailableColors();

    void showOrderColor(PackColor color);

    void printWholeOrderInfo(Gift gift);
}

