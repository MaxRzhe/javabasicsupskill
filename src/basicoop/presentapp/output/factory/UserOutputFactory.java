package basicoop.presentapp.output.factory;

import basicoop.presentapp.output.UserOutputWriter;

public interface UserOutputFactory {
    UserOutputWriter createOutputWriter();
}
