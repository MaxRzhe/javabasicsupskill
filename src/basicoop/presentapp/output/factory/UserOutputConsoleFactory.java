package basicoop.presentapp.output.factory;

import basicoop.presentapp.output.UserOutputConsoleWriter;
import basicoop.presentapp.output.UserOutputWriter;

public class UserOutputConsoleFactory implements UserOutputFactory{
    @Override
    public UserOutputWriter createOutputWriter() {
        return new UserOutputConsoleWriter();
    }
}
