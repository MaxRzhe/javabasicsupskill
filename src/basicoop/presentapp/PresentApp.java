package basicoop.presentapp;


import basicoop.presentapp.enums.PackColor;
import basicoop.presentapp.enums.PackType;
import basicoop.presentapp.enums.SweetType;
import basicoop.presentapp.input.UserInputHandler;
import basicoop.presentapp.input.factory.UserInputScannerFactory;
import basicoop.presentapp.model.Gift;
import basicoop.presentapp.model.Pack;
import basicoop.presentapp.model.Sweet;
import basicoop.presentapp.offers.OffersHandler;
import basicoop.presentapp.output.UserOutputHandler;
import basicoop.presentapp.output.factory.UserOutputConsoleFactory;

import java.util.*;
import java.util.stream.Collectors;

public class PresentApp {

    public static void main(String[] args) throws InterruptedException {
        UserInputHandler inputHandler = new UserInputHandler(new UserInputScannerFactory());
        UserOutputHandler outputHandler = new UserOutputHandler(new UserOutputConsoleFactory());

        boolean isOrderFinished = false;

        while (!isOrderFinished) {
            Map<String, Sweet> sweetMap = new HashMap<>();
            boolean isMore = true;
            while (isMore) {
                OffersHandler.sweetOffer();
                outputHandler.showAvailableSweets();

                String sweetType = inputHandler.getUserSweetChoice();

                OffersHandler.amountOffer();
                double amount = inputHandler.getUserAmountChoice();

                outputHandler.showSweetPreselection(sweetType, amount);

                fillSweetMap(sweetMap, sweetType, amount);

                OffersHandler.moreSweetsOffer();
                isMore = inputHandler.getUserMoreChoice();
            }

            outputHandler.showOrderedSweets(sweetMap);

            List<PackType> availablePacks = findAvailablePacks(sweetMap);

            if (availablePacks.size() == 0) {
                Thread.sleep(1000);
                OffersHandler.startAgain();
                Thread.sleep(2000);
            } else {
                OffersHandler.packOffer();
                outputHandler.showAvailablePacks(availablePacks);
                PackType packType = inputHandler.getUserPackChoice(availablePacks);
                outputHandler.showOrderedPack(packType);

                OffersHandler.packColorOffer();
                outputHandler.showAvailableColors();
                PackColor color = inputHandler.getUserPackColorChoice();
                outputHandler.showOrderColor(color);

                OffersHandler.giftTitleOffer();
                String giftTitle = inputHandler.getUserTitleChoice();

                inputHandler.closeUserInput();

                Gift gift = formGift(sweetMap, packType, color, giftTitle);

                outputHandler.printWholeOrderInfo(gift);

                isOrderFinished = true;
            }
        }
    }

    private static List<PackType> findAvailablePacks(Map<String, Sweet> sweetMap) {
        double wholeWeight = sweetMap.values().stream()
                .mapToDouble(Sweet::getAmountGrams)
                .sum();

        return Arrays.stream(PackType.values())
                .filter(packType -> packType.getCapacityGrams() >= wholeWeight)
                .collect(Collectors.toList());
    }

    private static void fillSweetMap(Map<String, Sweet> sweetMap, String sweetType, double amount) {
        sweetMap.merge(sweetType, new Sweet(SweetType.valueOf(sweetType), amount),
                (sweet, sweet2) -> {
                    sweet2.setAmountGrams(sweet.getAmountGrams() + sweet2.getAmountGrams());
                    return sweet2;
                });
    }

    private static Gift formGift(Map<String, Sweet> sweetMap, PackType packType, PackColor color, String giftTitle) {
        return new Gift(giftTitle, new ArrayList<>(sweetMap.values()), new Pack(packType, color));
    }

}
