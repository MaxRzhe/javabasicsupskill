package basicoop.presentapp.offers;

public abstract class OffersHandler {
    private static final String CHOOSE_SWEET_OFFER = "Please enter letters to choose a sweet type:";
    private static final String CHOOSE_AMOUNT_OFFER = "Please enter the required amount in grams:";
    private static final String CHOOSE_MORE_OFFER = "Would you like more sweets? Y/N";
    private static final String CHOOSE_PACK_OFFER = "Please choose a pack size from available: ";
    private static final String CHOOSE_PACK_COLOR_OFFER = "Please choose the pack color: ";
    private static final String CHOOSE_GIFT_TITLE_OFFER = "Please enter the gift title:(for example \"For Daddy with love\")";
    private static final String START_AGAIN_OFFER = "There are no packs available for this amount of sweets. " +
            "Max pack capacity 1600 grams.\nLet's start choosing again!\n\n";

    public static void sweetOffer() {
        System.out.println(CHOOSE_SWEET_OFFER);
    }

    public static void amountOffer() {
        System.out.println(CHOOSE_AMOUNT_OFFER);
    }

    public static void moreSweetsOffer() {
        System.out.println(CHOOSE_MORE_OFFER);
    }

    public static void packOffer() {
        System.out.println(CHOOSE_PACK_OFFER);
    }

    public static void packColorOffer() {
        System.out.println(CHOOSE_PACK_COLOR_OFFER);
    }

    public static void giftTitleOffer() {
        System.out.println(CHOOSE_GIFT_TITLE_OFFER);
    }

    public static void startAgain() {
        System.out.println(START_AGAIN_OFFER);
    }
}
