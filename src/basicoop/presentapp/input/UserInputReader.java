package basicoop.presentapp.input;

import basicoop.presentapp.enums.PackColor;
import basicoop.presentapp.enums.PackType;

import java.util.List;

public interface UserInputReader {

    String readSweetType();

    double readAmount();

    boolean readYesNo();

    PackType readPackType(List<PackType> availablePacks);

    PackColor readPackColor();

    String readGiftTitle();

    void close();
}
