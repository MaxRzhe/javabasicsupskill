package basicoop.presentapp.input;

import basicoop.presentapp.enums.PackColor;
import basicoop.presentapp.enums.PackType;
import basicoop.presentapp.enums.SweetType;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class UserInputScannerReader implements UserInputReader {
    private static final Scanner scanner;

    static {
        scanner = new Scanner(System.in);
    }

    @Override
    public String readSweetType() {
        String sweetType = "";
        boolean isValidInput = false;

        while (!isValidInput) {
            if (scanner.hasNext()) {
                String shortCut = scanner.next();
                if (SweetType.findByShortCut(shortCut) != null) {
                    sweetType = SweetType.findByShortCut(shortCut).name();
                    isValidInput = true;
                } else {
                    System.out.println("Invalid input. Try again");
                }
            }
        }
        return sweetType;
    }

    @Override
    public double readAmount() {
        double amount = 0.0;
        boolean isValidInput = false;

        while (!isValidInput) {
            if (scanner.hasNext()) {
                try {
                    amount = Double.parseDouble(scanner.next());
                    if (amount <= 0) {
                        System.out.println("Amount must be greater than O. Try again:");
                        continue;
                    }
                    isValidInput = true;
                } catch (NumberFormatException e) {
                    System.out.println("Invalid input. Try again:");
                }
            }
        }
        return amount;
    }

    @Override
    public boolean readYesNo() {
        boolean isYes = false;
        boolean isValidInput = false;
        while (!isValidInput) {
            if (scanner.hasNext()) {
                String s = scanner.next();
                if (s.equals("Y") || s.equals("N")) {
                    isYes = s.equals("Y");
                    isValidInput = true;
                } else {
                    System.out.println("Invalid input.Try again:");
                }
            }
        }
        return isYes;
    }

    @Override
    public PackType readPackType(List<PackType> availablePacks) {
        PackType packType = null;
        boolean isValidInput = false;
        while (!isValidInput) {
            if (scanner.hasNext()) {
                try {
                    int type = Integer.parseInt(scanner.next());
                    packType = availablePacks.stream()
                            .filter(pack -> pack.ordinal() == type)
                            .findFirst()
                            .orElse(null);
                    if (packType == null) {
                        System.out.println("Unavailable number. Try again:");
                        continue;
                    }
                    isValidInput = true;
                } catch (NumberFormatException e) {
                    System.out.println("Invalid input.Try again:");
                }
            }
        }
        return packType;
    }

    @Override
    public PackColor readPackColor() {
        PackColor color = null;
        boolean isValidInput = false;
        while (!isValidInput) {
            if (scanner.hasNext()) {
                try {
                    int colorNum = Integer.parseInt(scanner.next());
                    color = Arrays.stream(PackColor.values())
                            .filter(packColor -> packColor.ordinal() == colorNum)
                            .findFirst()
                            .orElse(null);
                    if (color == null) {
                        System.out.println("Unavailable number. Try again:");
                        continue;
                    }
                    isValidInput = true;
                } catch (NumberFormatException e) {
                    System.out.println("Invalid input.Try again:");
                }
            }
        }
        return color;
    }

    @Override
    public String readGiftTitle() {
        String title = "";
        boolean isValidInput = false;
        while (!isValidInput) {
            if (scanner.hasNext() && !(title = scanner.nextLine()).isEmpty()) {
                isValidInput = true;
            }
        }
        return title;
    }

    @Override
    public void close() {
        scanner.close();
    }
}
