package basicoop.presentapp.input.factory;

import basicoop.presentapp.input.UserInputReader;
import basicoop.presentapp.input.UserInputScannerReader;

public class UserInputScannerFactory implements UserInputFactory {

    @Override
    public UserInputReader createInputReader() {
        return new UserInputScannerReader();
    }
}
