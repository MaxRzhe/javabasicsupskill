package basicoop.presentapp.input.factory;

import basicoop.presentapp.input.UserInputReader;

public interface UserInputFactory {
    UserInputReader createInputReader();
}
