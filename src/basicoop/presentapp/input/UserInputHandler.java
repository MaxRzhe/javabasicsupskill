package basicoop.presentapp.input;

import basicoop.presentapp.enums.PackColor;
import basicoop.presentapp.enums.PackType;
import basicoop.presentapp.input.factory.UserInputFactory;

import java.util.List;

public class UserInputHandler {
    private final UserInputReader reader;

    public UserInputHandler(UserInputFactory factory) {
        this.reader = factory.createInputReader();
    }

    public double getUserAmountChoice() {
        return reader.readAmount();
    }

    public String getUserSweetChoice() {
        return reader.readSweetType();
    }

    public boolean getUserMoreChoice() {
        return reader.readYesNo();
    }

    public PackType getUserPackChoice(List<PackType> availablePacks) {
        return reader.readPackType(availablePacks);
    }

    public PackColor getUserPackColorChoice() {
        return reader.readPackColor();
    }

    public String getUserTitleChoice() {
        return reader.readGiftTitle();
    }

    public void closeUserInput() {
        reader.close();
    }





}
