package basicoop.presentapp.enums;

public enum PackType {
    XS(100, 1.99),
    S(300, 3.99),
    M(500,5.99),
    L(750, 7.99),
    XL(1100, 9.99),
    XXL(1500, 11.99);

    private final double capacityGrams;
    private final double price;

    PackType(double capacityGrams, double price) {
        this.capacityGrams = capacityGrams;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public double getCapacityGrams() {
        return capacityGrams;
    }
}
