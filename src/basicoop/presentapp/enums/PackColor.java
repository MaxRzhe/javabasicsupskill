package basicoop.presentapp.enums;

public enum PackColor {
    WHITE, GREEN, GREY, BLACK, BLUE, RED, YELLOW, ORANGE
}
