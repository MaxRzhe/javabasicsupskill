package basicoop.presentapp.enums;

import java.util.Arrays;

public enum SweetType {
    CANDY(3.99) {
        @Override
        public String shortCut() {
            return "CA";
        }
    },
    COOKIE(4.99) {
        @Override
        public String shortCut() {
            return "CO";
        }
    },
    MARMALADE(3.49) {
        @Override
        public String shortCut() {
            return "ML";
        }
    },
    MARSHMALLOW(5.39) {
        @Override
        public String shortCut() {
            return "MM";
        }
    },
    HALVA(7.49) {
        @Override
        public String shortCut() {
            return "HA";
        }
    },
    CHOCOLATE(9.79) {
        @Override
        public String shortCut() {
            return "CH";
        }
    };

    private final double pricePerKg;

    SweetType(double pricePerKg) {
        this.pricePerKg = pricePerKg;
    }

    public double getPricePerKg() {
        return pricePerKg;
    }

    public static SweetType findByShortCut(String shortCut) {
        return Arrays.stream(SweetType.values())
                .filter(type -> type.shortCut().equals(shortCut))
                .findFirst()
                .orElse(null);
    }

    public abstract String shortCut();

}
