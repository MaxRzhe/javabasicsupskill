package basicoop.fileapp;

public class DummyTextFile extends AbstractDummyFiles{
    private static final String NAME_PATTERN = "\\w+\\.txt";

    private final StringBuffer value;

    public DummyTextFile(String path, String name) throws DummyException {
        super(path, name);
        this.value = new StringBuffer();
    }

    @Override
    public long size() {
        return value.length();
    }

    @Override
    public String getNamePattern() {
        return NAME_PATTERN;
    }

    public String getContent() {
        return value.toString();
    }

    public void printContent() {
        System.out.println(value);
    }

    public void add(String line) {
        value.append(line);
    }

    public void clear() {
        value.setLength(0);
    }
}
