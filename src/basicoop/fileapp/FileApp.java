package basicoop.fileapp;

//Создать объект класса Текстовый файл, используя классы Файл, Директория.
//Методы: создать, переименовать, вывести на консоль содержимое, дополнить, удалить.
public class FileApp {

    public static void main(String[] args) {
        try {
            DummyDirectory directory = new DummyDirectory("module5/solving/", "fileapp");
            DummyTextFile textFile = new DummyTextFile("module5/solvings/fileapp/", "text.txt");

            textFile.add("I'm going to finish the last module!\n");
            textFile.add("It was very amazing study process. My mentor was very helpful.\n");

            directory.addFile(textFile);

            System.out.println(directory.size());
            textFile.rename("newFile.txt");

            directory.listFiles().forEach(file -> System.out.printf("*****\nFile name: %s\nFile content: %s\n*****\n",
                    file.getName(), file.getContent()));

            System.out.println(textFile.size());

            directory.deleteFile(textFile);

        } catch (DummyException e) {
            System.out.println(e.getMessage());
        }

    }
}
