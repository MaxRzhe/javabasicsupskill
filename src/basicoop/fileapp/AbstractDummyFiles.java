package basicoop.fileapp;

import java.time.LocalDateTime;

public abstract class AbstractDummyFiles {
    public static final String PATH_PATTERN = "(\\w+/)+";

    private String name;
    private String path;
    private long size;
    private LocalDateTime lastModified;

    public AbstractDummyFiles(String path, String name) throws DummyException {
        if (!path.matches(PATH_PATTERN)) {
            throw new DummyException("Invalid path");
        }
        if (!name.matches(getNamePattern())) {
            throw new DummyException("Invalid name");
        } else {
            this.path = path;
            this.name = name;
            this.size = 0L;
            this.lastModified = LocalDateTime.now();
        }
    }

    public String getName() {
        return name;
    }

    public void rename(String name) {
        if (name.matches(getNamePattern())) {
            this.name = name;
            this.lastModified = LocalDateTime.now();
        }
    }

    public String getPath() {
        return path;
    }

    public void relocate(String path) {
        if (path.matches(PATH_PATTERN)) {
            this.path = path;
            this.lastModified = LocalDateTime.now();
        }
    }

    public long getSize() {
        return size;
    }

    public LocalDateTime lastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public abstract long size();

    public abstract String getNamePattern();
}
