package basicoop.fileapp;

public class DummyException extends Exception{
    private final String message;

    public DummyException(String message) {
        super();
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
