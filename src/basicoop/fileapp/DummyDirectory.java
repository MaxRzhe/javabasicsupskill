package basicoop.fileapp;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class DummyDirectory extends AbstractDummyFiles{
    private static final String NAME_PATTERN = "\\w+";

    private final Set<DummyTextFile> files;

    public DummyDirectory(String path, String name) throws DummyException {
        super(path, name);
        this.files = new HashSet<>();
    }

    @Override
    public String getNamePattern() {
        return NAME_PATTERN;
    }

    @Override
    public long size() {
        return files.size();
    }

    public Set<DummyTextFile> listFiles() {
        return files;
    }

    public boolean addFile(DummyTextFile file) {
        if (files.add(file)) {
            setLastModified(LocalDateTime.now());
            return true;
        }
        return false;
    }

    public boolean deleteFile(DummyTextFile file) {
        if (files.remove(file)) {
            setLastModified(LocalDateTime.now());
            return true;
        }
        return false;
    }

    public void clear() {
        files.clear();
    }
}
