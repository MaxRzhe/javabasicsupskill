package basicoop.calendarapp;


import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;

//Создать класс Календарь с внутренним классом,
//с помощью объектов которого можно хранить информацию о выходных и праздничных днях.
public class SchedulerApp {

    public static void main(String[] args) {
        LocalDate startDay = LocalDate.of(2021, Month.JANUARY, 1);
        LocalDate finishDay = LocalDate.of(2021, Month.MAY, 31);

        CalendarScheduler calendar = new CalendarScheduler(startDay, finishDay);
        CalendarScheduler.DaysOff daysOff = calendar.new DaysOff();

        daysOff.setDayOff(LocalDate.of(2021, Month.JANUARY, 1));
        daysOff.setDayOff(LocalDate.of(2021, Month.JANUARY, 7));
        daysOff.setDayOff(LocalDate.of(2021, Month.MARCH, 8));
        daysOff.setDayOff(LocalDate.of(2021, Month.MAY, 1));
        daysOff.setDayOff(LocalDate.of(2021, Month.MAY, 9));

        daysOff.setWeeklyDaysOff(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);

        System.out.printf("Всего дней по графику: %d\n", calendar.totalDaysAmount());
        System.out.println("Из них:");
        System.out.printf("\tрабочих -> %d\n", calendar.totalDaysAmount() - daysOff.daysOffAmount());
        System.out.printf("\tвыходных -> %d\n", daysOff.daysOffAmount());
    }
}
