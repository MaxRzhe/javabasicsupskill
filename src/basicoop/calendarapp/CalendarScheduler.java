package basicoop.calendarapp;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CalendarScheduler {
    private LocalDate startDate;
    private LocalDate finishDate;
    private List<LocalDate> dayList;
    private final long daysAmount;

    public CalendarScheduler(LocalDate startDate, LocalDate finishDate) {
        this.startDate = startDate.isBefore(finishDate) ? startDate : finishDate;
        this.finishDate = startDate.isBefore(finishDate) ? finishDate : startDate;
        fillDayList(startDate, finishDate);
        this.daysAmount = !dayList.isEmpty() ? dayList.size() : 0;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getFinishDate() {
        return finishDate;
    }

    public void setStartDate(LocalDate startDate) {
        if (startDate.isAfter(finishDate)) {
            this.startDate = startDate;
        }
    }

    public void setFinishDate(LocalDate finishDate) {
        if (finishDate.isBefore(startDate)) {
            this.finishDate = finishDate;
        }
    }

    public List<LocalDate> getDayList() {
        return dayList;
    }

    public long totalDaysAmount() {
        return daysAmount;
    }

    private void fillDayList(LocalDate startDate, LocalDate finishDate) {
        dayList = startDate.datesUntil(finishDate.plusDays(1))
                .sorted()
                .collect(Collectors.toList());
    }

    public class DaysOff {
        private final Set<LocalDate> daysOff;

        public DaysOff() {
            this.daysOff = new HashSet<>();
        }

        public Set<LocalDate> getDaysOff() {
            return daysOff;
        }

        public void setDayOff(LocalDate date) {
            if (date.compareTo(startDate) >= 0 && date.compareTo(finishDate) <= 0) {
                daysOff.add(date);
            }
        }

        public void setWeeklyDaysOff(LocalDate fromDate, LocalDate toDate, DayOfWeek... daysOfWeek) {
            List<DayOfWeek> dayOffList = Arrays.stream(daysOfWeek).collect(Collectors.toList());
            if ((fromDate.compareTo(startDate) >= 0 && fromDate.compareTo(finishDate) <= 0) &&
                    (toDate.compareTo(startDate) >= 0 && toDate.compareTo(finishDate) <= 0) &&
                    fromDate.isBefore(toDate) && !dayOffList.isEmpty()) {
                List<LocalDate> dateList = dayList.stream().filter(date -> date.compareTo(fromDate) >= 0 && date.compareTo(toDate) <= 0
                        && dayOffList.contains(date.getDayOfWeek()))
                        .collect(Collectors.toList());
                daysOff.addAll(dateList);
            }
        }

        public void setWeeklyDaysOff(DayOfWeek... daysOfWeek) {
            setWeeklyDaysOff(startDate, finishDate, daysOfWeek);
        }

        public boolean isDayOff(LocalDate date) {
            return daysOff.contains(date);
        }

        public long daysOffAmount() {
            return daysOff.size();
        }
    }

}

