package basics.linearprograms;

public class Task3 {

    //Вычислить значение выражения по формуле (все переменные принимают действительные значения):
    //  (sin(x) + cos(y)) / (cos(x) - sin(y)) * tg(xy)
    public static double findFuncValue(double x, double y) {
        double radX = Math.toRadians(x);
        double radY = Math.toRadians(y);
        double radXY = Math.toRadians(x * y);
        return (Math.sin(radX) + Math.cos(radY)) / (Math.cos(radX) - Math.sin(radY)) * Math.tan(radXY);
    }
}
