package basics.linearprograms;

public class Task6 {

    //Для данной области составить линейную программу,
    //которая печатает true, если точка с координатами (х, у) принадлежит закрашенной области,
    //и false — в противном случае
    public static boolean isDotInside(int x, int y) {
        return (x >= -4 && x <= 4) &&
                (y >= -3 && y <= 4) &&
                (x >= -2 || y <= 0) &&
                (x <= 2 || y <= 0);
    }
}
