package basics.linearprograms;

public class Task4 {

    //Дано действительное число R вида nnn.ddd (три цифровых разряда в дробной и целой частях).
    // Поменять местами дробную и целую части числа и вывести полученное значение числа
    public static void printChangedDouble(double d) {
        String[] numArr = Double.toString(d).split("\\.");
        System.out.println(numArr[1] + "." + numArr[0]);
    }
}
