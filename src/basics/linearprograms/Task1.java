package basics.linearprograms;

public class Task1 {

    //Найдите  значение функции: z = ( (a – 3 ) * b / 2) + c
    public static double findFunctionValue(double a, double b, double c) {
        return ((a - 3) * b / 2) + c;
    }
}
