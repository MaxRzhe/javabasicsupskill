package basics.cycles;

public class Task2 {

    //Вычислить значения функции на отрезке [а,b] c шагом h:
    //http://comp-science.narod.ru/executants/images/vich/vca1.jpg
    public static void calcFunction(int a, int b, int h) {
        for (int i = a; i <= b; i += h) {
            if (a > 2) {
                System.out.println(a);
            } else {
                System.out.println(-a);
            }
        }
    }
}
