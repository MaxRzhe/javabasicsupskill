package basics.cycles;

import java.math.BigInteger;
import java.util.stream.LongStream;

public class Task4 {

    //Составить программу нахождения произведения квадратов первых двухсот чисел
    public static String productSquaresTwoHundredNumbers() {
        return LongStream.rangeClosed(1, 200)
                .map(num -> num * num)
                .mapToObj(BigInteger::valueOf)
                .reduce(BigInteger::multiply)
                .orElse(BigInteger.ZERO).toString();
    }
}
