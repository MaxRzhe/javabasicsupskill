package basics.cycles;

public class Task3 {

    // Найти сумму квадратов первых ста чисел
    public static int sumOfSquaresFirstHundredNumbers() {
        int x = 1;
        int y = 100;
        return  (y * (y + 1) * (2 * y + 1)) / 6 - (x * (x-1) * (2 * x - 1)) / 6;
    }
}
