package basics.branching;

public class Task4 {

    //Заданы размеры А, В прямоугольного отверстия и размеры х, у, z кирпича.
    // Определить, пройдет ли кирпич через отверстие
    public static boolean isBrickFitHole(int A, int B, int x, int y, int z) {
        if (A == x) {
            return B == y || B == z;
        } else if (A == y) {
            return B == x || B == z;
        } else if (A == z) {
            return B == x || B == y;
        }
        return false;
    }

}
