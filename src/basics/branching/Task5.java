package basics.branching;

public class Task5 {

    //Вычислить значение функции:
    public static double calculateFunction(double x) {
        if (x <= 3) {
            return x * x - 3 * x + 9;
        } else {
            return 1 / (x * x * x + 6);
        }
    }
}
